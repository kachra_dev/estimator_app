import 'package:flutter/material.dart';

class AppTheme {
  static ThemeData get light {
    return ThemeData(
      appBarTheme: const AppBarTheme(
        elevation: 0,
        color: Colors.white,
      ),
      scaffoldBackgroundColor: Colors.white,
      primaryColor: Colors.white,
      accentColor: const Color(0xFF004AAD),
      splashColor: Colors.transparent,
      focusColor: Colors.grey,
      fontFamily: 'Poppins',
      textTheme: const TextTheme(
        headline5: TextStyle(fontSize: 20.0, height: 1.35),
        headline4: TextStyle(
            fontSize: 18.0, fontWeight: FontWeight.w600, height: 1.35),
        headline3: TextStyle(
            fontSize: 20.0,
            fontWeight: FontWeight.w600,
            height: 1.35,
            color: Colors.black),
        headline2: TextStyle(
            fontSize: 22.0, fontWeight: FontWeight.w700, height: 1.35),
        headline1:
            TextStyle(fontSize: 22.0, fontWeight: FontWeight.w300, height: 1.5),
        subtitle1: TextStyle(
            fontSize: 15.0, fontWeight: FontWeight.w500, height: 1.35),
        headline6: TextStyle(
            fontSize: 16.0, fontWeight: FontWeight.w600, height: 1.35),
        bodyText2: TextStyle(fontSize: 12.0, height: 1.35),
        bodyText1: TextStyle(fontSize: 14.0, height: 1.35),
        caption: TextStyle(fontSize: 12.0, height: 1.35),
      ),
    );
  }
}
