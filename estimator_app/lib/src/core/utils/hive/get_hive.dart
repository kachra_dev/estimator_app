import 'package:estimator_app/src/data/models/hive/driver_hive.dart';
import 'package:estimator_app/src/data/models/hive/profile_hive.dart';
import 'package:hive/hive.dart';

class GetHive {
  final userBox = Hive.box('userProfile');

  Future<ProfileHive> getCurrentUserProfile() async {
    return userBox.getAt(0) as ProfileHive;
  }
}
