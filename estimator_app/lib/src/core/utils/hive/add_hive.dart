import 'package:estimator_app/src/data/models/hive/driver_hive.dart';
import 'package:estimator_app/src/data/models/hive/profile_hive.dart';
import 'package:hive/hive.dart';

class AddHive {
  final userBox = Hive.box('userProfile');

  void addUser(ProfileHive profileHive) {
    userBox.add(profileHive);
  }
}
