import 'package:emojis/emojis.dart';
import 'package:flutter/material.dart';

class TextChecker{
  checkText(String text){
    if(text.length > 12){
      return text.replaceRange(12, text.length, '...');
    }

    return text;
  }
}

const String confirmEmoji = Emojis.thinkingFace;
const String yesEmoji = Emojis.checkBoxWithCheck;
const String noEmoji = Emojis.crossMark;

const TextDirection ltrTextDirection = TextDirection.ltr;