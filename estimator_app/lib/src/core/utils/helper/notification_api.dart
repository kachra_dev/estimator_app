import 'dart:io';

import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;
import 'package:rxdart/rxdart.dart';

class FlutterLocalNotificationModel{
  final String title;
  final String body;
  final String asset;
  final String? payload;
  FlutterLocalNotificationModel({required this.title, required this.body, required this.asset, this.payload});
}

class NotificationApi{
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  static BehaviorSubject<String?> onNotifications = BehaviorSubject<String?>();

  Future<void> initNotification({bool initScheduled = false}) async {
    const AndroidInitializationSettings initializationSettingsAndroid = AndroidInitializationSettings('@mipmap/launcher_icon');
    const IOSInitializationSettings initializationSettingsIOS = IOSInitializationSettings();
    const InitializationSettings initializationSettings = InitializationSettings(android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onSelectNotification: (payload) async {
        onNotifications.add(payload);
      }
    );
  }

  Future<String> _downloadAndSaveFile(String url, String fileName) async {
    final Directory directory = await getApplicationDocumentsDirectory();
    final String filePath = '${directory.path}/$fileName';
    final http.Response response = await http.get(Uri.parse(url));
    final File file = File(filePath);
    await file.writeAsBytes(response.bodyBytes);
    return filePath;
  }

  Future<void> showNotification(FlutterLocalNotificationModel flutterLocalNotificationModel) async {
    final String largeIconPath = await _downloadAndSaveFile(
        'https://firebasestorage.googleapis.com/v0/b/kachra-78757.appspot.com/o/drivers_app_assets%2Fsplash.png?alt=media&token=ab266278-2ee5-4cf0-9fd8-b6e34e954666', 'largeIcon');
    final String bigPicturePath = await _downloadAndSaveFile(
        flutterLocalNotificationModel.asset, 'bigPicture');
    final BigPictureStyleInformation bigPictureStyleInformation =
    BigPictureStyleInformation(
        FilePathAndroidBitmap(bigPicturePath),
        largeIcon: FilePathAndroidBitmap(largeIconPath),
        contentTitle: flutterLocalNotificationModel.title,
        htmlFormatContentTitle: true,
        summaryText: flutterLocalNotificationModel.body,
        htmlFormatSummaryText: true);
    final AndroidNotificationDetails androidPlatformChannelSpecifics =
    AndroidNotificationDetails(
        'kchra',
        'kchra-channel',
        channelDescription: 'Kchra Notifications for Drivers',
        styleInformation: bigPictureStyleInformation,
        importance: Importance.max,
        priority: Priority.high,
        ticker: 'ticker',
        sound: const RawResourceAndroidNotificationSound('new_bookings_notif'),
        playSound: true
    );
    final NotificationDetails platformChannelSpecifics =
    NotificationDetails(android: androidPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
      0,
      flutterLocalNotificationModel.title,
      flutterLocalNotificationModel.body,
      platformChannelSpecifics,
      payload: flutterLocalNotificationModel.payload,
    );

    // const AndroidNotificationDetails androidPlatformChannelSpecifics =
    // AndroidNotificationDetails(
    //     'kchra',
    //     'kchra-channel',
    //     channelDescription: 'Kchra Notifications for Drivers',
    //     importance: Importance.max,
    //     priority: Priority.high,
    //     ticker: 'ticker',
    //     sound: RawResourceAndroidNotificationSound('new_bookings_notif'),
    //     playSound: true
    // );
    // const NotificationDetails platformChannelSpecifics = NotificationDetails(android: androidPlatformChannelSpecifics);
    // await flutterLocalNotificationsPlugin.show(
    //     0,
    //     flutterLocalNotificationModel.title,
    //     flutterLocalNotificationModel.body,
    //     platformChannelSpecifics
    // );
  }
}