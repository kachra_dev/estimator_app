import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CopyClipboard{
  Future<void> copyToClipboard(BuildContext context, String text) async {
    await Clipboard.setData(ClipboardData(text: text));
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      content: Text('Booking ID copied to clipboard!'),
    ));
  }
}