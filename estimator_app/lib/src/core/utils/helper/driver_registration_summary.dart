import 'package:estimator_app/src/data/models/address/address_model.dart';

class DriverRegistrationSummary {
  String name;
  String email;
  String password;
  String passwordConfirmation;
  String phoneNumber;
  AddressModel address;
  String country;
  String base64Passport;
  String base64DriversLicense;
  String base64Istimara;
  String base64CarInsurance;
  String base64VehiclePhoto;
  String base64LicensePlateNumber;

  DriverRegistrationSummary(
      {required this.name,
      required this.email,
      required this.password,
      required this.passwordConfirmation,
      required this.phoneNumber,
      required this.address,
      required this.country,
      required this.base64Passport,
      required this.base64DriversLicense,
      required this.base64Istimara,
      required this.base64CarInsurance,
      required this.base64VehiclePhoto,
      required this.base64LicensePlateNumber});
}

String kPathPassport = '';
String kPathDriversLicense = '';
String kPathIstimara = '';
String kPathCarInsurance = '';
String kPathVehiclePhoto = '';
String kPathLicensePlateNumber = '';

class DriverRegistrationPhotoPath {
  setPathPassport(String any) {
    kPathPassport = any;
  }

  setPathDriversLicense(String any) {
    kPathDriversLicense = any;
  }

  setPathIstimara(String any) {
    kPathIstimara = any;
  }

  setPathCarInsurance(String any) {
    kPathCarInsurance = any;
  }

  setPathVehiclePhoto(String any) {
    kPathVehiclePhoto = any;
  }

  setPathLicensePlateNumber(String any) {
    kPathLicensePlateNumber = any;
  }
}

AddressModel driverAddress = AddressModel(
    name: '',
    street: '',
    ISOCountryCode: '',
    administrativeArea: '',
    country: '',
    locality: '',
    latitude: '',
    longitude: '');

DriverRegistrationSummary driverRegistrationSummary = DriverRegistrationSummary(
    name: '',
    email: '',
    password: '',
    passwordConfirmation: '',
    phoneNumber: '',
    address: driverAddress,
    country: '',
    base64Passport: '',
    base64DriversLicense: '',
    base64Istimara: '',
    base64CarInsurance: '',
    base64VehiclePhoto: '',
    base64LicensePlateNumber: '');

class StoreDriversRegistration {
  setName(String any) {
    driverRegistrationSummary.name = any;
  }

  setEmail(String any) {
    driverRegistrationSummary.email = any;
  }

  setPassword(String any) {
    driverRegistrationSummary.password = any;
  }

  setPasswordConfirmation(String any) {
    driverRegistrationSummary.passwordConfirmation = any;
  }

  setPhoneNumber(String any) {
    driverRegistrationSummary.phoneNumber = any;
  }

  setCountry(String any) {
    driverRegistrationSummary.country = any;
  }

  setBase64Passport(String any) {
    driverRegistrationSummary.base64Passport = any;
  }

  setBase64DriversLicense(String any) {
    driverRegistrationSummary.base64DriversLicense = any;
  }

  setBase64hIstimara(String any) {
    driverRegistrationSummary.base64Istimara = any;
  }

  setBase64CarInsurance(String any) {
    driverRegistrationSummary.base64CarInsurance = any;
  }

  setBase64VehiclePhoto(String any) {
    driverRegistrationSummary.base64VehiclePhoto = any;
  }

  setBase64LicensePlateNumber(String any) {
    driverRegistrationSummary.base64LicensePlateNumber = any;
  }
}

class ClearDriverRegistration {
  clearDriverRegistration() {
    driverRegistrationSummary.name = '';
    driverRegistrationSummary.email = '';
    driverRegistrationSummary.password = '';
    driverRegistrationSummary.passwordConfirmation = '';
    driverRegistrationSummary.address = driverAddress;
    driverRegistrationSummary.country = '';
    driverRegistrationSummary.phoneNumber = '';
    driverRegistrationSummary.base64Passport = '';
    driverRegistrationSummary.base64DriversLicense = '';
    driverRegistrationSummary.base64Istimara = '';
    driverRegistrationSummary.base64CarInsurance = '';
    driverRegistrationSummary.base64VehiclePhoto = '';
    driverRegistrationSummary.base64LicensePlateNumber = '';

    kPathPassport = '';
    kPathDriversLicense = '';
    kPathIstimara = '';
    kPathCarInsurance = '';
    kPathVehiclePhoto = '';
    kPathLicensePlateNumber = '';
  }
}
