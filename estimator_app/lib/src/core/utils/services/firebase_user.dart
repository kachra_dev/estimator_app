import 'metadata.dart';

class FirebaseUser {
  final String? displayName;
  final String? email;
  final String? emailVerified;
  final String? isAnonymous;
  final UserMetadata? metadata;
  final String? phoneNumber;
  final String? photoUrl;

  FirebaseUser(
      {this.displayName,
        this.email,
        this.emailVerified,
        this.isAnonymous,
        this.metadata,
        this.phoneNumber,
        this.photoUrl});
}
