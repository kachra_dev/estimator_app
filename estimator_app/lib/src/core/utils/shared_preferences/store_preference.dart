import 'package:shared_preferences/shared_preferences.dart';

class StorePreference{
  Future<void> storeAccessToken(String accessToken) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('accessToken', accessToken);
  }
}