import 'package:estimator_app/src/core/utils/services/firebase_service.dart';
import 'package:hive/hive.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LogOut {
  Future<void> logOut() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    ///remove accessToken
    sharedPreferences.remove('accessToken');

    ///remove saved profile
    final userBox = Hive.box('userProfile');
    userBox.deleteAt(0);

    //sign out from Google Sign In, if necessary
    await FirebaseService().signOutFromGoogle();
  }
}
