import 'package:estimator_app/translations/locale_keys.g.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

class SuccessMessage extends StatelessWidget {
  final String title;
  final String content;
  final Function onPressedFunction;

  const SuccessMessage(
      {Key? key,
      required this.title,
      required this.content,
      required this.onPressedFunction})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        title,
        style: const TextStyle(fontWeight: FontWeight.w900),
      ),
      content: SizedBox(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Image.asset('assets/img/success.png', scale: 3.0),
              const SizedBox(height: 20.0),
              Text(
                content,
                style: const TextStyle(
                    fontWeight: FontWeight.w500, fontSize: 12.0),
              )
            ],
          ),
        ),
      ),
      actions: [
        TextButton(
            onPressed: () {
              onPressedFunction();
            },
            child: Text(LocaleKeys.close.tr()))
      ],
    );
  }
}
