import 'package:estimator_app/src/data/models/address/address_model.dart';

class AuthParams {
  final String email;
  final String password;

  AuthParams({required this.email, required this.password});
}

class RegisterAuthParams {
  final String name;
  final String email;
  final String password;
  final String passwordConfirmation;
  final String role;
  final AddressModel address;
  final String country;
  final String phoneNumber;
  RegisterAuthParams(
      {required this.name,
      required this.email,
      required this.password,
      required this.passwordConfirmation,
      required this.role,
      required this.address,
      required this.country,
      required this.phoneNumber});
}

class RegisterGoogleAuthParams {
  final String name;
  final String email;
  final String role;
  final AddressModel address;
  final String country;
  final String phoneNumber;
  RegisterGoogleAuthParams(
      {required this.name,
      required this.email,
      required this.role,
      required this.address,
      required this.country,
      required this.phoneNumber});
}

class RegisterAssetAuthParams {
  final String driverId;
  final String asset;
  final String assetName;

  RegisterAssetAuthParams(
      {required this.driverId, required this.asset, required this.assetName});
}

class DriverAvailabilityParams {
  final String driverId;
  final String availabilityStatus;

  DriverAvailabilityParams(
      {required this.driverId, required this.availabilityStatus});
}
