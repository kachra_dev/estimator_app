class BookingParams{
  final String? userId;
  final String? bookingStatus;
  final String? asset;

  BookingParams({this.userId, this.bookingStatus, this.asset});
}

class ChangeStatusBookingParams{
  final String? bookingId;
  final String? bookingStatus;

  ChangeStatusBookingParams({this.bookingId, this.bookingStatus});
}

class SetIncompleteDetailsBookingParams{
  final String? bookingId;
  final String? incompleteMessage;

  SetIncompleteDetailsBookingParams({this.bookingId, this.incompleteMessage});
}

class AcceptDeclineBookingParams{
  final String? bookingId;
  final String? driverBookingStatus;

  AcceptDeclineBookingParams({this.bookingId, this.driverBookingStatus});

}

class CreateExtraKchraParams{
  final String? bookingId;
  final String? additionalAssetType;
  final String? asset;

  CreateExtraKchraParams(
      {this.bookingId, this.additionalAssetType, this.asset});
}

class SetEstimatedAmountParams{
  final String? bookingId;
  final int? estimatedAmount;

  SetEstimatedAmountParams({this.bookingId, this.estimatedAmount});
}