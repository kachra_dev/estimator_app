import 'package:estimator_app/src/core/params/booking_params.dart';
import 'package:estimator_app/src/core/resources/data_state.dart';
import 'package:estimator_app/src/core/usecase/usecase.dart';
import 'package:estimator_app/src/data/models/booking/estimator_booking_model.dart';
import 'package:estimator_app/src/data/models/general_response.dart';
import 'package:estimator_app/src/domain/repositories/booking_repository/booking_repository.dart';

class GetAllBookingUseCase
    implements UseCase<DataState<List<EstimatorBookingModel>>, void> {
  final BookingRepository bookingRepository;

  GetAllBookingUseCase(this.bookingRepository);

  @override
  Future<DataState<List<EstimatorBookingModel>>> call({void params}) {
    return bookingRepository.getAllBookings();
  }
}

class FindBookingUseCase implements UseCase<DataState<EstimatorBookingModel>, String?> {
  final BookingRepository bookingRepository;

  FindBookingUseCase(this.bookingRepository);

  @override
  Future<DataState<EstimatorBookingModel>> call({String? params}) {
    return bookingRepository.findBooking(params);
  }
}

class SetEstimatedAmountUseCase implements UseCase<DataState<EstimatorBookingModel>, SetEstimatedAmountParams?> {
  final BookingRepository bookingRepository;

  SetEstimatedAmountUseCase(this.bookingRepository);

  @override
  Future<DataState<EstimatorBookingModel>> call({SetEstimatedAmountParams? params}) {
    return bookingRepository.setEstimatedAmount(params);
  }
}
