import 'package:estimator_app/src/core/resources/data_state.dart';
import 'package:estimator_app/src/core/usecase/usecase.dart';
import 'package:estimator_app/src/data/models/site_visit_schedules/detailed_site_visit_schedule_model.dart';
import 'package:estimator_app/src/data/models/site_visit_schedules/site_visit_schedules_model.dart';
import 'package:estimator_app/src/domain/repositories/site_visit_repository/site_visit_repository.dart';

class SiteVisitScheduleUseCase implements UseCase<DataState<List<SiteVisitSchedulesModel>>, void> {
  final SiteVisitRepository siteVisitRepository;

  SiteVisitScheduleUseCase(this.siteVisitRepository);

  @override
  Future<DataState<List<SiteVisitSchedulesModel>>> call({void params}) {
    return siteVisitRepository.getAllSiteVisitSchedules();
  }
}

class FindSiteVisitScheduleUseCase implements UseCase<DataState<DetailedSiteVisitScheduleModel>, String?> {
  final SiteVisitRepository siteVisitRepository;

  FindSiteVisitScheduleUseCase(this.siteVisitRepository);

  @override
  Future<DataState<DetailedSiteVisitScheduleModel>> call({String? params}) {
    return siteVisitRepository.findSiteVisitSchedule(params);
  }
}