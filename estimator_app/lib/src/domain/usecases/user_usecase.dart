import 'package:estimator_app/src/core/params/auth_params.dart';
import 'package:estimator_app/src/core/resources/data_state.dart';
import 'package:estimator_app/src/core/usecase/usecase.dart';
import 'package:estimator_app/src/data/models/auth/auth_model.dart';
import 'package:estimator_app/src/data/models/auth/driver_model.dart';
import 'package:estimator_app/src/data/models/auth/register_auth_model.dart';
import 'package:estimator_app/src/data/models/check_driver_assets_model.dart';
import 'package:estimator_app/src/domain/repositories/user_repository/user_repository.dart';

class UserLoginUseCase implements UseCase<DataState<AuthModel>, AuthParams?> {
  final UserRepository userRepository;

  UserLoginUseCase(this.userRepository);
  @override
  Future<DataState<AuthModel>> call({AuthParams? params}) {
    return userRepository.login(params);
  }
}

class UserLoginGoogleUseCase implements UseCase<DataState<AuthModel>, String?> {
  final UserRepository userRepository;

  UserLoginGoogleUseCase(this.userRepository);
  @override
  Future<DataState<AuthModel>> call({String? params}) {
    return userRepository.loginGoogle(params);
  }
}

class RegisterDriverUseCase
    implements UseCase<DataState<DriverModel>, RegisterAuthParams?> {
  final UserRepository userRepository;

  RegisterDriverUseCase(this.userRepository);
  @override
  Future<DataState<DriverModel>> call({RegisterAuthParams? params}) {
    return userRepository.registerDriver(params);
  }
}

class RegisterDriverGoogleUseCase
    implements UseCase<DataState<DriverModel>, RegisterGoogleAuthParams?> {
  final UserRepository userRepository;

  RegisterDriverGoogleUseCase(this.userRepository);
  @override
  Future<DataState<DriverModel>> call({RegisterGoogleAuthParams? params}) {
    return userRepository.registerDriverUsingGoogle(params);
  }
}

class RegisterDriverAssetUseCase
    implements UseCase<DataState<String>, RegisterAssetAuthParams?> {
  final UserRepository userRepository;

  RegisterDriverAssetUseCase(this.userRepository);

  @override
  Future<DataState<String>> call({RegisterAssetAuthParams? params}) {
    return userRepository.registerDriverAsset(params);
  }
}

class CheckDriverAssetUseCase
    implements UseCase<DataState<CheckDriversAssetsModel>, String?> {
  final UserRepository userRepository;

  CheckDriverAssetUseCase(this.userRepository);

  @override
  Future<DataState<CheckDriversAssetsModel>> call({String? params}) {
    return userRepository.checkDriverAsset(params);
  }
}

class ChangeDriverAvailabilityUseCase
    implements UseCase<DataState<DriverModel>, DriverAvailabilityParams?> {
  final UserRepository userRepository;

  ChangeDriverAvailabilityUseCase(this.userRepository);

  @override
  Future<DataState<DriverModel>> call({DriverAvailabilityParams? params}) {
    return userRepository.changeDriverAvailability(params);
  }
}
