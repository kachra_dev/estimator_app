import 'package:estimator_app/src/core/resources/data_state.dart';
import 'package:estimator_app/src/data/models/site_visit_schedules/detailed_site_visit_schedule_model.dart';
import 'package:estimator_app/src/data/models/site_visit_schedules/site_visit_schedules_model.dart';

abstract class SiteVisitRepository {
  Future<DataState<List<SiteVisitSchedulesModel>>> getAllSiteVisitSchedules();
  Future<DataState<DetailedSiteVisitScheduleModel>> findSiteVisitSchedule(String? siteVisitScheduleId);
}
