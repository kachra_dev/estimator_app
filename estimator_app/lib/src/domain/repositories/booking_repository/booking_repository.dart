import 'package:estimator_app/src/core/params/booking_params.dart';
import 'package:estimator_app/src/core/resources/data_state.dart';
import 'package:estimator_app/src/data/models/booking/estimator_booking_model.dart';
import 'package:estimator_app/src/data/models/general_response.dart';

abstract class BookingRepository {
  // API methods
  Future<DataState<List<EstimatorBookingModel>>> getAllBookings();
  Future<DataState<EstimatorBookingModel>> findBooking(String? params);
  Future<DataState<EstimatorBookingModel>> setEstimatedAmount(SetEstimatedAmountParams? params);
}
