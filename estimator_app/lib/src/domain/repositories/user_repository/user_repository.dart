import 'package:estimator_app/src/core/params/auth_params.dart';
import 'package:estimator_app/src/core/resources/data_state.dart';
import 'package:estimator_app/src/data/models/auth/auth_model.dart';
import 'package:estimator_app/src/data/models/auth/driver_model.dart';
import 'package:estimator_app/src/data/models/auth/register_auth_model.dart';
import 'package:estimator_app/src/data/models/check_driver_assets_model.dart';

abstract class UserRepository {
  // API methods
  Future<DataState<AuthModel>> login(AuthParams? params);
  Future<DataState<AuthModel>> loginGoogle(String? params);
  Future<DataState<DriverModel>> registerDriver(RegisterAuthParams? params);
  Future<DataState<DriverModel>> registerDriverUsingGoogle(
      RegisterGoogleAuthParams? registerGoogleAuthParams);
  Future<DataState<String>> registerDriverAsset(
      RegisterAssetAuthParams? params);
  Future<DataState<CheckDriversAssetsModel>> checkDriverAsset(String? params);
  Future<DataState<DriverModel>> changeDriverAvailability(
      DriverAvailabilityParams? params);
}
