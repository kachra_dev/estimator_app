import 'package:estimator_app/src/data/models/address/address_model.dart';
import 'package:estimator_app/src/data/models/hive/profile_hive.dart';
import 'package:estimator_app/src/data/models/profile/profile_model.dart';

class EstimatorBookingModel {
  final String? id;
  final String? userId;
  final int? estimatedAmount;
  final String? bookingStatus;
  final String? quoteStatus;
  final String? assetType;
  final String? asset;
  final String? updatedAt;
  final ProfileHive? user;
  final AddressModel? address;
  final String? paymentMethod;
  final String? driverBookingStatus;
  final int? hasAdditionalKachra;
  final String? bookingDetails;
  final int? additionalAmount;
  final String? bookingPurpose;

  EstimatorBookingModel(
      {this.id,
      this.userId,
      this.estimatedAmount,
      this.bookingStatus,
      this.quoteStatus,
      this.assetType,
      this.asset,
      this.updatedAt,
      this.user,
      this.address,
      this.paymentMethod,
      this.driverBookingStatus,
      this.hasAdditionalKachra,
      this.bookingDetails,
      this.additionalAmount,
      this.bookingPurpose
    });

  factory EstimatorBookingModel.fromJson(Map<String, dynamic> json) {
    return EstimatorBookingModel(
      id: json['id'] as String?,
      userId: json['user_id'] as String?,
      estimatedAmount: json['estimated_amount'] as int?,
      bookingStatus: json['booking_status'] as String?,
      quoteStatus: json['quote_status'] as String?,
      assetType: json['asset_type'] as String?,
      asset: json['asset'] as String?,
      updatedAt: json['updated_at'] as String?,
      user: json['user'] != null ? ProfileHive.fromJson(json['user']) : null,
      address: json['address'] != null ? AddressModel.fromJson(json['address']) : null,
      paymentMethod: json['payment_method'] as String?,
      driverBookingStatus: json['driver_booking_status'] as String?,
      hasAdditionalKachra: json['has_additional_kchra'] as int?,
      bookingDetails: json['booking_details'] as String?,
      additionalAmount: json['additional_amount'] as int?,
      bookingPurpose: json['booking_purpose'] as String?
    );
  }
}
