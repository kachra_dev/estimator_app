import 'package:estimator_app/src/data/models/hive/profile_hive.dart';
import 'package:hive/hive.dart';

import 'address_hive.dart';

part 'driver_hive.g.dart';

@HiveType(typeId: 2)
class DriverHive {
  @HiveField(0)
  final String? id;

  @HiveField(1)
  final String? userId;

  @HiveField(2)
  final String? availabilityStatus;

  @HiveField(3)
  final String? driverAccountStatus;

  DriverHive(
      {this.id,
      this.userId,
      this.availabilityStatus,
      this.driverAccountStatus});

  factory DriverHive.fromJson(Map<String, dynamic> json) {
    return DriverHive(
        id: json['id'] as String?,
        userId: json['user_id'] as String?,
        availabilityStatus: json['availability_status'] as String?,
        driverAccountStatus: json['driver_account_status'] as String?);
  }
}
