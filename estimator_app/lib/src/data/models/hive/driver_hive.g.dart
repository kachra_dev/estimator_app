// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'driver_hive.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class DriverHiveAdapter extends TypeAdapter<DriverHive> {
  @override
  final int typeId = 2;

  @override
  DriverHive read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return DriverHive(
      id: fields[0] as String?,
      userId: fields[1] as String?,
      availabilityStatus: fields[2] as String?,
      driverAccountStatus: fields[3] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, DriverHive obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.userId)
      ..writeByte(2)
      ..write(obj.availabilityStatus)
      ..writeByte(3)
      ..write(obj.driverAccountStatus);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DriverHiveAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
