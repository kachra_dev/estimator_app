class AddressModel {
  final String? id;
  final String? name;
  final String? street;
  final String? ISOCountryCode;
  final String? country;
  final String? administrativeArea;
  final String? locality;
  final String? latitude;
  final String? longitude;

  AddressModel(
      {this.id,
      this.name,
      this.street,
      this.ISOCountryCode,
      this.country,
      this.administrativeArea,
      this.locality,
      this.latitude,
      this.longitude});

  factory AddressModel.fromJson(Map<String, dynamic> json) {
    return AddressModel(
        id: json['id'] as String?,
        name: json['name'] as String?,
        street: json['street'] as String?,
        ISOCountryCode: json['ISO_country_code'] as String?,
        country: json['country'] as String?,
        administrativeArea: json['administrative_area'] as String?,
        locality: json['locality'] as String?,
        latitude: json['latitude'] as String?,
        longitude: json['longitude'] as String?
    );
  }

  Map<String, dynamic> toJson() => {
    'name': name,
    'street': street,
    'ISO_country_code': ISOCountryCode,
    'country': country,
    'administrative_area': administrativeArea,
    'locality': locality,
    'latitude': latitude,
    'longitude': longitude
  };
}