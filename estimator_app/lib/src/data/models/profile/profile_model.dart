import 'package:estimator_app/src/data/models/address/address_model.dart';

class ProfileModel {
  final String? id;
  final String? name;
  final String? email;
  final Map<String, dynamic>? address;
  final String? phoneNumber;
  final String? country;
  final String? role;

  ProfileModel(
      {required this.id,
      required this.name,
      required this.email,
      required this.address,
      required this.phoneNumber,
      required this.country,
      required this.role});

  factory ProfileModel.fromJson(Map<String, dynamic> json) {
    return ProfileModel(
      id: json['id'] as String?,
      name: json['name'] as String?,
      email: json['email'] as String?,
      phoneNumber: json['phone_number'] as String?,
      country: json['country'] as String?,
      address: json['address'] as Map<String, dynamic>,
      role: json['role'] as String?,
    );
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "email": email,
        "address": address,
        "phone_number": phoneNumber,
        "country": country,
        "role": role
      };
}
