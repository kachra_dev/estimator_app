import 'package:estimator_app/src/data/models/profile/profile_model.dart';

class DriverModel {
  final String? userId;
  final String? availabilityStatus;
  final String? driverAccountStatus;
  final String? id;

  DriverModel(
      {this.userId,
      this.availabilityStatus,
      this.driverAccountStatus,
      this.id});

  factory DriverModel.fromJson(Map<String, dynamic> json) {
    return DriverModel(
      userId: json['user_id'] as String?,
      availabilityStatus: json['availability_status'] as String?,
      driverAccountStatus: json['driver_account_status'] as String?,
      id: json['id'] as String?,
    );
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "availability_status": availabilityStatus,
        "driver_account_status": driverAccountStatus,
      };
}
