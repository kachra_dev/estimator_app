import 'package:estimator_app/src/data/models/auth/driver_model.dart';
import 'package:estimator_app/src/data/models/profile/profile_model.dart';

class AuthModel {
  final String? accessToken;
  final String? tokenType;
  final int? expiresIn;
  final DriverModel? driver;
  final ProfileModel? user;

  AuthModel(
      {this.accessToken,
      this.tokenType,
      this.expiresIn,
      this.driver,
      this.user});

  factory AuthModel.fromJson(Map<String, dynamic> json) {
    return AuthModel(
        accessToken: json['access_token'] as String?,
        tokenType: json['token_type'] as String?,
        expiresIn: json['expires_in'] as int?,
        driver: json['driver'] != null
            ? DriverModel.fromJson(json['driver'])
            : json['driver'],
        user: json['user'] != null
            ? ProfileModel.fromJson(json['user'])
            : json['user']);
  }
}
