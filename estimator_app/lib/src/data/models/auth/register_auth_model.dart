import 'package:estimator_app/src/data/models/profile/profile_model.dart';

class RegisterAuthModel {
  final String? message;
  final ProfileModel? user;

  RegisterAuthModel({this.message, this.user});

  factory RegisterAuthModel.fromJson(Map<String, dynamic> json) {
    return RegisterAuthModel(
        message: json['message'] as String?,
        user: ProfileModel.fromJson(json['user']));
  }
}
