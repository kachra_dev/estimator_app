class SiteVisitSchedulesModel{
  final String? id;
  final DateTime? scheduledDate;
  final String? userId;
  final String? addressId;
  final String? scheduleStatus;

  SiteVisitSchedulesModel({this.id, this.scheduledDate, this.userId, this.addressId, this.scheduleStatus});

  factory SiteVisitSchedulesModel.fromJson(Map<String, dynamic> json){
    return SiteVisitSchedulesModel(
      id: json['id'] as String?,
      scheduledDate: json['scheduled_date'] != null ? DateTime.parse(json['scheduled_date']) : null,
      userId: json['user_id'] as String?,
      addressId: json['address'] as String?,
      scheduleStatus: json['schedule_status'] as String?
    );
  }
}