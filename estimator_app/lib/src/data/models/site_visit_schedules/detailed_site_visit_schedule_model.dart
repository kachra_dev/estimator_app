import 'package:estimator_app/src/data/models/address/address_model.dart';
import 'package:estimator_app/src/data/models/profile/profile_model.dart';

class DetailedSiteVisitScheduleModel{
  final String? id;
  final DateTime? scheduledDate;
  final String? userId;
  final AddressModel? address;
  final ProfileModel? user;

  DetailedSiteVisitScheduleModel({this.id, this.scheduledDate, this.userId, this.address, this.user});

  factory DetailedSiteVisitScheduleModel.fromJson(Map<String, dynamic> json){
    return DetailedSiteVisitScheduleModel(
      id: json['id'] as String?,
      scheduledDate: json['scheduled_date'] != null ? DateTime.parse(json['scheduled_date']) : null,
      userId: json['user_id'] as String?,
      address: json['address'] != null ? AddressModel.fromJson(json['address']) : null,
      user: json['user'] != null ? ProfileModel.fromJson(json['user']) : null,
    );
  }
}