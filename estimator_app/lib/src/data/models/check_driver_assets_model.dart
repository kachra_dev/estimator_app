class CheckDriversAssetsModel{
  final bool carInsurance;
  final bool driversLicense;
  final bool istimara;
  final bool licensePlate;
  final bool passport;
  final bool vehicle;

  CheckDriversAssetsModel(
      {required this.carInsurance,
      required this.driversLicense,
      required this.istimara,
      required this.licensePlate,
      required this.passport,
      required this.vehicle});

  factory CheckDriversAssetsModel.fromJson(Map<String, dynamic> json){
    return CheckDriversAssetsModel(
        carInsurance: json['car_insurance'] as bool,
        driversLicense: json['drivers_license'] as bool,
        istimara: json['istimara'] as bool,
        licensePlate: json['license_plate'] as bool,
        passport: json['passport'] as bool,
        vehicle: json['vehicle'] as bool
    );
  }

  Map<String, dynamic> toJson() => {
    'car_insurance': carInsurance,
    'drivers_license': driversLicense,
    'istimara': istimara,
    'license_plate': licensePlate,
    'passport': passport,
    'vehicle': vehicle,
  };
}