class GeneralResponse{
  final String? message;

  GeneralResponse({this.message});

  factory GeneralResponse.fromJson(Map<String, dynamic> json){
    return GeneralResponse(
      message: json['message'] as String?
    );
  }
}