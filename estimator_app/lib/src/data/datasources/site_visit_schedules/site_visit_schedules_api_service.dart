import 'package:dio/dio.dart';
import 'package:estimator_app/src/core/utils/constants.dart';
import 'package:estimator_app/src/data/models/auth/driver_model.dart';
import 'package:estimator_app/src/data/models/booking/estimator_booking_model.dart';
import 'package:estimator_app/src/data/models/general_response.dart';
import 'package:estimator_app/src/data/models/site_visit_schedules/detailed_site_visit_schedule_model.dart';
import 'package:estimator_app/src/data/models/site_visit_schedules/site_visit_schedules_model.dart';
import 'package:retrofit/retrofit.dart';

part 'site_visit_schedules_api_service.g.dart';

@RestApi(baseUrl: kLaravelUrl)
abstract class SiteVisitSchedulesApiService {
  factory SiteVisitSchedulesApiService(Dio dio, {String baseUrl}) = _SiteVisitSchedulesApiService;

  @GET('/estimator/get-ongoing-schedules')
  Future<HttpResponse<List<SiteVisitSchedulesModel>>> getAllSiteVisitSchedules();

  @POST('/estimator/find-site-visit-schedule')
  Future<HttpResponse<DetailedSiteVisitScheduleModel>> findSiteVisitSchedule(
      @Field("id") String? siteVisitScheduleId,
  );
}
