import 'package:dio/dio.dart';
import 'package:estimator_app/src/core/utils/constants.dart';
import 'package:estimator_app/src/data/models/auth/driver_model.dart';
import 'package:estimator_app/src/data/models/booking/estimator_booking_model.dart';
import 'package:estimator_app/src/data/models/general_response.dart';
import 'package:retrofit/retrofit.dart';

part 'booking_api_service.g.dart';

@RestApi(baseUrl: kLaravelUrl)
abstract class BookingApiService {
  factory BookingApiService(Dio dio, {String baseUrl}) = _BookingApiService;

  @POST('/booking/get-bookings-estimator')
  Future<HttpResponse<List<EstimatorBookingModel>>> getAllBookings();

  @POST('/booking/find-booking')
  Future<HttpResponse<EstimatorBookingModel>> findBooking(
    @Field("id") String? bookingId,
  );

  @PUT('/booking/set-estimated-amount')
  Future<HttpResponse<EstimatorBookingModel>> setEstimatedAmount(
      @Field("id") String? bookingId,
      @Field("estimated_amount") int? estimatedAmount,
  );
}
