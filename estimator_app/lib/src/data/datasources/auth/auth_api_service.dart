import 'package:dio/dio.dart';
import 'package:estimator_app/src/core/utils/constants.dart';
import 'package:estimator_app/src/data/models/address/address_model.dart';
import 'package:estimator_app/src/data/models/auth/auth_model.dart';
import 'package:estimator_app/src/data/models/auth/driver_model.dart';
import 'package:estimator_app/src/data/models/auth/register_auth_model.dart';
import 'package:estimator_app/src/data/models/check_driver_assets_model.dart';
import 'package:retrofit/retrofit.dart';

part 'auth_api_service.g.dart';

@RestApi(baseUrl: kLaravelUrl)
abstract class AuthApiService {
  factory AuthApiService(Dio dio, {String baseUrl}) = _AuthApiService;

  @POST('/auth/login')
  Future<HttpResponse<AuthModel>> login(
      @Field("email") String? email, @Field("password") String? password);

  @POST('/auth/login-google')
  Future<HttpResponse<AuthModel>> loginGoogle(@Field("email") String? email);

  @POST('/drivers/register-driver')
  Future<HttpResponse<DriverModel>> registerDriver(
      @Field("name") String? name,
      @Field("email") String? email,
      @Field("password") String? password,
      @Field("password_confirmation") String? passwordConfirmation,
      @Field("role") String? role,
      @Field("address") AddressModel? address,
      @Field("country") String? country,
      @Field("phone_number") String? phoneNumber);

  @POST('/drivers/register-driver-asset')
  Future<HttpResponse<String>> registerDriverAsset(
      @Field("driver_id") String? driverId,
      @Field("asset") String? asset,
      @Field("asset_name") String? assetName);

  @POST('/drivers/check-driver-asset')
  Future<HttpResponse<CheckDriversAssetsModel>> checkDriverAssets(
      @Field("driver_id") String? driverId);

  @POST('/drivers/driver-availability')
  Future<HttpResponse<DriverModel>> changeDriverAvailability(
    @Field("driver_id") String? driverId,
    @Field("availability_status") String? availabilityStatus,
  );

  @POST('/drivers/register-driver-using-google')
  Future<HttpResponse<DriverModel>> registerDriverUsingGoogle(
    @Field("name") String? name,
    @Field("email") String? email,
    @Field("address") AddressModel? address,
    @Field("phone_number") String? phoneNumber,
    @Field("country") String? country,
    @Field("role") String? role,
  );
}
