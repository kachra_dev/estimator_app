import 'dart:io';

import 'package:dio/dio.dart';
import 'package:estimator_app/src/core/params/booking_params.dart';
import 'package:estimator_app/src/core/resources/data_state.dart';
import 'package:estimator_app/src/data/datasources/booking/booking_api_service.dart';
import 'package:estimator_app/src/data/models/booking/estimator_booking_model.dart';
import 'package:estimator_app/src/data/models/general_response.dart';
import 'package:estimator_app/src/domain/repositories/booking_repository/booking_repository.dart';
import 'package:logger/logger.dart';

class BookingRepositoryImpl extends BookingRepository {
  final BookingApiService bookingApiService;

  BookingRepositoryImpl(this.bookingApiService);

  @override
  Future<DataState<List<EstimatorBookingModel>>> getAllBookings() async {
    var logger = Logger();

    try {
      final httpResponse = await bookingApiService.getAllBookings();

      logger.i(httpResponse);

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }

  @override
  Future<DataState<EstimatorBookingModel>> findBooking(String? params) async {
    var logger = Logger();

    try {
      final httpResponse = await bookingApiService.findBooking(params);

      logger.i(httpResponse);

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }

  @override
  Future<DataState<EstimatorBookingModel>> setEstimatedAmount(SetEstimatedAmountParams? params) async {
    var logger = Logger();

    try {
      final httpResponse = await bookingApiService.setEstimatedAmount(params!.bookingId, params.estimatedAmount);

      logger.i(httpResponse);

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }
}
