import 'dart:io';

import 'package:dio/dio.dart';
import 'package:estimator_app/src/core/resources/data_state.dart';
import 'package:estimator_app/src/data/datasources/booking/booking_api_service.dart';
import 'package:estimator_app/src/data/datasources/site_visit_schedules/site_visit_schedules_api_service.dart';
import 'package:estimator_app/src/data/models/booking/estimator_booking_model.dart';
import 'package:estimator_app/src/data/models/site_visit_schedules/detailed_site_visit_schedule_model.dart';
import 'package:estimator_app/src/data/models/site_visit_schedules/site_visit_schedules_model.dart';
import 'package:estimator_app/src/domain/repositories/site_visit_repository/site_visit_repository.dart';
import 'package:logger/logger.dart';

class SiteVisitRepositoryImpl extends SiteVisitRepository {
  final SiteVisitSchedulesApiService siteVisitSchedulesApiService;

  SiteVisitRepositoryImpl(this.siteVisitSchedulesApiService);

  @override
  Future<DataState<List<SiteVisitSchedulesModel>>> getAllSiteVisitSchedules() async {
    var logger = Logger();

    try {
      final httpResponse = await siteVisitSchedulesApiService.getAllSiteVisitSchedules();

      logger.i(httpResponse);

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }

  @override
  Future<DataState<DetailedSiteVisitScheduleModel>> findSiteVisitSchedule(String? siteVisitScheduleId) async {
    var logger = Logger();

    try {
      final httpResponse = await siteVisitSchedulesApiService.findSiteVisitSchedule(siteVisitScheduleId);

      logger.i(httpResponse);

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }
}
