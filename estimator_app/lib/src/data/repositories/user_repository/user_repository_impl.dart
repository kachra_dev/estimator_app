import 'dart:io';

import 'package:dio/dio.dart';
import 'package:estimator_app/src/core/params/auth_params.dart';
import 'package:estimator_app/src/core/resources/data_state.dart';
import 'package:estimator_app/src/data/datasources/auth/auth_api_service.dart';
import 'package:estimator_app/src/data/models/auth/auth_model.dart';
import 'package:estimator_app/src/data/models/auth/driver_model.dart';
import 'package:estimator_app/src/data/models/auth/register_auth_model.dart';
import 'package:estimator_app/src/data/models/check_driver_assets_model.dart';
import 'package:estimator_app/src/domain/repositories/user_repository/user_repository.dart';
import 'package:logger/logger.dart';

class UserRepositoryImpl extends UserRepository {
  final AuthApiService authApiService;

  UserRepositoryImpl(this.authApiService);

  @override
  Future<DataState<AuthModel>> login(AuthParams? params) async {
    var logger = Logger();

    logger.i(params);

    try {
      final httpResponse =
          await authApiService.login(params!.email, params.password);

      logger.i(httpResponse);

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }

  @override
  Future<DataState<DriverModel>> registerDriver(
      RegisterAuthParams? params) async {
    var logger = Logger();

    logger.i(params);

    try {
      final httpResponse = await authApiService.registerDriver(
          params!.name,
          params.email,
          params.password,
          params.passwordConfirmation,
          'Driver',
          params.address,
          params.country,
          params.phoneNumber);

      logger.i(httpResponse);

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }

  @override
  Future<DataState<String>> registerDriverAsset(
      RegisterAssetAuthParams? params) async {
    var logger = Logger();

    logger.i(params);

    try {
      final httpResponse = await authApiService.registerDriverAsset(
          params!.driverId, params.asset, params.assetName);

      logger.i(httpResponse);

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }

  @override
  Future<DataState<CheckDriversAssetsModel>> checkDriverAsset(
      String? params) async {
    var logger = Logger();

    logger.i(params);

    try {
      final httpResponse = await authApiService.checkDriverAssets(params);

      logger.i(httpResponse);

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }

  @override
  Future<DataState<DriverModel>> changeDriverAvailability(
      DriverAvailabilityParams? params) async {
    var logger = Logger();

    logger.i(params);

    try {
      final httpResponse = await authApiService.changeDriverAvailability(
          params!.driverId, params.availabilityStatus);

      logger.i(httpResponse);

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }

  @override
  Future<DataState<AuthModel>> loginGoogle(String? params) async {
    var logger = Logger();

    logger.i(params);

    try {
      final httpResponse = await authApiService.loginGoogle(params!);

      logger.i(httpResponse);

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }

  @override
  Future<DataState<DriverModel>> registerDriverUsingGoogle(
      RegisterGoogleAuthParams? registerGoogleAuthParams) async {
    var logger = Logger();

    logger.i(registerGoogleAuthParams);

    try {
      final httpResponse = await authApiService.registerDriverUsingGoogle(
          registerGoogleAuthParams!.name,
          registerGoogleAuthParams.email,
          registerGoogleAuthParams.address,
          registerGoogleAuthParams.phoneNumber,
          registerGoogleAuthParams.country,
          'Driver');

      logger.i(httpResponse);

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }
}
