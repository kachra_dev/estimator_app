import 'package:dio/dio.dart';
import 'package:estimator_app/src/data/datasources/auth/auth_api_service.dart';
import 'package:estimator_app/src/data/datasources/booking/booking_api_service.dart';
import 'package:estimator_app/src/data/datasources/site_visit_schedules/site_visit_schedules_api_service.dart';
import 'package:estimator_app/src/data/repositories/booking_repository/booking_repository_impl.dart';
import 'package:estimator_app/src/data/repositories/site_visit_repository/site_visit_repository_impl.dart';
import 'package:estimator_app/src/data/repositories/user_repository/user_repository_impl.dart';
import 'package:estimator_app/src/domain/repositories/booking_repository/booking_repository.dart';
import 'package:estimator_app/src/domain/repositories/site_visit_repository/site_visit_repository.dart';
import 'package:estimator_app/src/domain/repositories/user_repository/user_repository.dart';
import 'package:estimator_app/src/domain/usecases/booking_usecase.dart';
import 'package:estimator_app/src/domain/usecases/user_usecase.dart';
import 'package:estimator_app/src/presentation/bloc/booking/booking_bloc.dart';
import 'package:estimator_app/src/presentation/bloc/booking/find_booking/find_booking_bloc.dart';
import 'package:estimator_app/src/presentation/bloc/booking/set_estimateed_amount/set_estimated_amount_bloc.dart';
import 'package:estimator_app/src/presentation/bloc/site_visit_schedules/find_site_visit_schedules/find_site_visit_schedules_bloc.dart';
import 'package:estimator_app/src/presentation/bloc/site_visit_schedules/site_visit_schedules_bloc.dart';
import 'package:get_it/get_it.dart';

import 'domain/usecases/site_visit_usecase.dart';
import 'presentation/bloc/auth/auth_bloc.dart';

final injector = GetIt.instance;

Future<void> initializeDependencies() async {
  // Dio client
  injector.registerSingleton<Dio>(Dio());

  // Dependencies and API services
  injector.registerSingleton<AuthApiService>(AuthApiService(injector()));
  injector.registerSingleton<BookingApiService>(BookingApiService(injector()));
  injector.registerSingleton<SiteVisitSchedulesApiService>(SiteVisitSchedulesApiService(injector()));

  //Repositories
  injector.registerSingleton<UserRepository>(UserRepositoryImpl(injector()));
  injector.registerSingleton<BookingRepository>(BookingRepositoryImpl(injector()));
  injector.registerSingleton<SiteVisitRepository>(SiteVisitRepositoryImpl(injector()));

  // UseCases
  injector.registerSingleton<UserLoginUseCase>(UserLoginUseCase(injector()));
  injector.registerSingleton<GetAllBookingUseCase>(GetAllBookingUseCase(injector()));
  injector.registerSingleton<RegisterDriverUseCase>(RegisterDriverUseCase(injector()));
  injector.registerSingleton<RegisterDriverAssetUseCase>(RegisterDriverAssetUseCase(injector()));
  injector.registerSingleton<CheckDriverAssetUseCase>(CheckDriverAssetUseCase(injector()));
  injector.registerSingleton<FindBookingUseCase>(FindBookingUseCase(injector()));
  injector.registerSingleton<ChangeDriverAvailabilityUseCase>(ChangeDriverAvailabilityUseCase(injector()));
  injector.registerSingleton<UserLoginGoogleUseCase>(UserLoginGoogleUseCase(injector()));
  injector.registerSingleton<RegisterDriverGoogleUseCase>(RegisterDriverGoogleUseCase(injector()));
  injector.registerSingleton<SiteVisitScheduleUseCase>(SiteVisitScheduleUseCase(injector()));
  injector.registerSingleton<FindSiteVisitScheduleUseCase>(FindSiteVisitScheduleUseCase(injector()));
  injector.registerSingleton<SetEstimatedAmountUseCase>(SetEstimatedAmountUseCase(injector()));

  // Blocs
  injector.registerFactory<AuthBloc>(() => AuthBloc(injector(), injector(), injector(), injector()));
  injector.registerFactory<BookingBloc>(() => BookingBloc(injector()));
  injector.registerFactory<FindBookingBloc>(() => FindBookingBloc(injector()));
  injector.registerFactory<SiteVisitSchedulesBloc>(() => SiteVisitSchedulesBloc(injector()));
  injector.registerFactory<FindSiteVisitSchedulesBloc>(() => FindSiteVisitSchedulesBloc(injector()));
  injector.registerFactory<SetEstimatedAmountBloc>(() => SetEstimatedAmountBloc(injector()));
}
