part of 'booking_bloc.dart';

abstract class BookingState extends Equatable {
  final List<EstimatorBookingModel>? bookingList;
  const BookingState({this.bookingList});

  @override
  List<Object> get props => [];
}

class LoadingBookingState extends BookingState {
  const LoadingBookingState();
}

class SuccessBookingState extends BookingState{
  final List<EstimatorBookingModel>? booking;
  const SuccessBookingState({this.booking}):super(bookingList: booking);
}

class ErrorBookingState extends BookingState{
  const ErrorBookingState();
}
