part of 'find_booking_bloc.dart';

abstract class FindBookingEvent extends Equatable {
  const FindBookingEvent();

  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class FindBookEvent extends FindBookingEvent{
  final String? bookingId;
  const FindBookEvent(this.bookingId);
}
