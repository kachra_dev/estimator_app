import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:estimator_app/src/data/models/booking/estimator_booking_model.dart';
import 'package:estimator_app/src/domain/usecases/booking_usecase.dart';
import 'package:equatable/equatable.dart';

part 'find_booking_event.dart';
part 'find_booking_state.dart';

class FindBookingBloc extends Bloc<FindBookingEvent, FindBookingState> {
  final FindBookingUseCase findBookingUseCase;
  FindBookingBloc(this.findBookingUseCase) : super(const FindBookingInitial());

  @override
  Stream<FindBookingState> mapEventToState(FindBookingEvent event) async* {
    if (event is FindBookEvent) {
      yield* _findBookingEvent(event.bookingId);
    }
  }

  Stream<FindBookingState> _findBookingEvent(String? bookingId) async* {
    yield const LoadingFindBookingState();

    if (bookingId != null) {
      final dataState = await findBookingUseCase(params: bookingId);

      if (dataState.data != null) {
        EstimatorBookingModel? driverBookingModel = dataState.data;

        yield SuccessFindBookingState(driverBookingModel);
      } else {
        yield const ErrorFindBookingState();
      }
    } else {
      yield const ErrorFindBookingState();
    }
  }
}
