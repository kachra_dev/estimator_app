part of 'find_booking_bloc.dart';

abstract class FindBookingState extends Equatable {
  final EstimatorBookingModel? estimatorBookingModelList;
  const FindBookingState({this.estimatorBookingModelList});

  @override
  List<Object?> get props => [];
}

class FindBookingInitial extends FindBookingState {
  const FindBookingInitial();
}

class LoadingFindBookingState extends FindBookingState{
  const LoadingFindBookingState();
}

class SuccessFindBookingState extends FindBookingState{
  final EstimatorBookingModel? estimatorBookingModel;
  const SuccessFindBookingState(this.estimatorBookingModel) : super(estimatorBookingModelList: estimatorBookingModel);
}

class ErrorFindBookingState extends FindBookingState{
  const ErrorFindBookingState();
}