part of 'booking_bloc.dart';

abstract class BookingEvent extends Equatable {
  const BookingEvent();

  @override
  List<Object?> get props => [];
}

class InitialBookingEvent extends BookingEvent{
  const InitialBookingEvent();
}

class CreateBookingEvent extends BookingEvent{
  final BookingParams? bookingParams;
  const CreateBookingEvent(this.bookingParams);
}

class GetAllBookingsEvent extends BookingEvent{
  const GetAllBookingsEvent();
}


