import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:estimator_app/src/core/params/booking_params.dart';
import 'package:estimator_app/src/data/models/booking/estimator_booking_model.dart';
import 'package:estimator_app/src/domain/usecases/booking_usecase.dart';
import 'package:equatable/equatable.dart';
import 'package:logger/logger.dart';

part 'booking_event.dart';
part 'booking_state.dart';

class BookingBloc extends Bloc<BookingEvent, BookingState> {
  final GetAllBookingUseCase getAllBookingUseCase;

  BookingBloc(this.getAllBookingUseCase)
      : super(const LoadingBookingState());

  @override
  Stream<BookingState> mapEventToState(BookingEvent event) async* {
    if (event is InitialBookingEvent) {
      yield* _initialBookingEvent();
    }

    if (event is GetAllBookingsEvent) {
      yield* _getAllBookings();
    }
  }

  Stream<BookingState> _initialBookingEvent() async* {
    yield const ErrorBookingState();
  }

  Stream<BookingState> _getAllBookings() async* {
    var logger = Logger();
    yield const LoadingBookingState();

    final dataState = await getAllBookingUseCase();

    logger.i(dataState);
    logger.i(dataState.data);

    if (dataState.data != null) {
      List<EstimatorBookingModel>? driverBookings = dataState.data;

      for (var data in driverBookings!) {
        logger.i(data.id);
      }
      yield SuccessBookingState(booking: driverBookings);
    } else {
      yield const ErrorBookingState();
    }
  }
}
