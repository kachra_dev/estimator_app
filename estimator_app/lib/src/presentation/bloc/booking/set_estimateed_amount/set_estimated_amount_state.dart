part of 'set_estimated_amount_bloc.dart';

abstract class SetEstimatedAmountState extends Equatable {
  final EstimatorBookingModel? estimatorBookingModelList;
  const SetEstimatedAmountState({this.estimatorBookingModelList});

  @override
  List<Object?> get props => [];
}

class SetEstimatedAmountInitial extends SetEstimatedAmountState {
  const SetEstimatedAmountInitial();
}

class LoadingSetEstimatedAmountState extends SetEstimatedAmountState{
  const LoadingSetEstimatedAmountState();
}
class SuccessSetEstimatedAmountState extends SetEstimatedAmountState{
  final EstimatorBookingModel? estimatorBookingModel;
  const SuccessSetEstimatedAmountState(this.estimatorBookingModel) : super(estimatorBookingModelList: estimatorBookingModel);
}
class ErrorSetEstimatedAmountState extends SetEstimatedAmountState{
  const ErrorSetEstimatedAmountState();
}
