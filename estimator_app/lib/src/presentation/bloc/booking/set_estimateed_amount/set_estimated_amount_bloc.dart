import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:estimator_app/main.dart';
import 'package:estimator_app/src/core/params/booking_params.dart';
import 'package:estimator_app/src/core/utils/messages/error_message.dart';
import 'package:estimator_app/src/core/utils/messages/success_message.dart';
import 'package:estimator_app/src/data/models/booking/estimator_booking_model.dart';
import 'package:estimator_app/src/domain/usecases/booking_usecase.dart';
import 'package:estimator_app/src/presentation/bloc/booking/booking_bloc.dart';
import 'package:estimator_app/translations/locale_keys.g.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'set_estimated_amount_event.dart';
part 'set_estimated_amount_state.dart';

class SetEstimatedAmountBloc extends Bloc<SetEstimatedAmountEvent, SetEstimatedAmountState> {
  final SetEstimatedAmountUseCase setEstimatedAmountUseCase;
  SetEstimatedAmountBloc(this.setEstimatedAmountUseCase) : super(const SetEstimatedAmountInitial());

  @override
  Stream<SetEstimatedAmountState> mapEventToState(SetEstimatedAmountEvent event) async* {
    if (event is SetAmountEvent) {
      yield* _setEstimatedAmount(event.setEstimatedAmountParams);
    }
  }

  Stream<SetEstimatedAmountState> _setEstimatedAmount(SetEstimatedAmountParams? setEstimatedAmountParams) async* {
    yield const LoadingSetEstimatedAmountState();

    if(setEstimatedAmountParams != null){
      final dataState = await setEstimatedAmountUseCase(params: setEstimatedAmountParams);

      if(dataState.data != null){
        EstimatorBookingModel estimatorBookingModel = dataState.data!;

        showDialog(
          context: MyApp.navigatorKey.currentContext!,
          builder: (context) => SuccessMessage(
            title: LocaleKeys.success.tr(),
            content: 'Successfully set Estimated Amount!',
            onPressedFunction: (){
              final bookingBloc = BlocProvider.of<BookingBloc>(context);
              bookingBloc.add(const GetAllBookingsEvent());

              Navigator.popUntil(context, (Route<dynamic> route) => route.isFirst);
            })
        );

        yield SuccessSetEstimatedAmountState(estimatorBookingModel);
      } else{
        showDialog(
            context: MyApp.navigatorKey.currentContext!,
            builder: (context) => ErrorMessage(
                title: LocaleKeys.error.tr(),
                content: 'Error setting Estimated Amount!',
                onPressedFunction: () => Navigator.pop(context))
        );

        yield const ErrorSetEstimatedAmountState();
      }
    } else{
      yield const ErrorSetEstimatedAmountState();
    }
  }
}
