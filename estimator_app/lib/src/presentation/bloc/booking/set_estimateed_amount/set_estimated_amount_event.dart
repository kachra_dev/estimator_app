part of 'set_estimated_amount_bloc.dart';

abstract class SetEstimatedAmountEvent extends Equatable {
  const SetEstimatedAmountEvent();

  @override
  List<Object?> get props => [];
}

class SetAmountEvent extends SetEstimatedAmountEvent{
  final SetEstimatedAmountParams? setEstimatedAmountParams;
  const SetAmountEvent(this.setEstimatedAmountParams);
}
