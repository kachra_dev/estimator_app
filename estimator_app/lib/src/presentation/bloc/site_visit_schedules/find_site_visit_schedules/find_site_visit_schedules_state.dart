part of 'find_site_visit_schedules_bloc.dart';

abstract class FindSiteVisitSchedulesState extends Equatable {
  final DetailedSiteVisitScheduleModel? detailedSiteVisitScheduleModelList;
  const FindSiteVisitSchedulesState({this.detailedSiteVisitScheduleModelList});

  @override
  List<Object?> get props => [];
}

class FindSiteVisitSchedulesInitial extends FindSiteVisitSchedulesState {
  const FindSiteVisitSchedulesInitial();
}

class LoadingFindSiteVisitScheduleState extends FindSiteVisitSchedulesState{
  const LoadingFindSiteVisitScheduleState();
}

class SuccessFindSiteVisitScheduleState extends FindSiteVisitSchedulesState{
  final DetailedSiteVisitScheduleModel? detailedSiteVisitScheduleModel;
  const SuccessFindSiteVisitScheduleState(this.detailedSiteVisitScheduleModel) : super(detailedSiteVisitScheduleModelList: detailedSiteVisitScheduleModel);
}

class ErrorFindSiteVisitScheduleState extends FindSiteVisitSchedulesState{
  const ErrorFindSiteVisitScheduleState();
}
