import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:estimator_app/src/data/models/site_visit_schedules/detailed_site_visit_schedule_model.dart';
import 'package:estimator_app/src/domain/usecases/site_visit_usecase.dart';

part 'find_site_visit_schedules_event.dart';
part 'find_site_visit_schedules_state.dart';

class FindSiteVisitSchedulesBloc extends Bloc<FindSiteVisitSchedulesEvent, FindSiteVisitSchedulesState> {
  final FindSiteVisitScheduleUseCase findSiteVisitScheduleUseCase;
  FindSiteVisitSchedulesBloc(this.findSiteVisitScheduleUseCase) : super(const FindSiteVisitSchedulesInitial());

  @override
  Stream<FindSiteVisitSchedulesState> mapEventToState(FindSiteVisitSchedulesEvent event) async* {
    if (event is FindSiteScheduleEvent) {
      yield* _findSiteSchedule(event.siteVisitScheduleId);
    }
  }

  Stream<FindSiteVisitSchedulesState> _findSiteSchedule(String? siteVisitScheduleId) async* {
    yield const LoadingFindSiteVisitScheduleState();

    if(siteVisitScheduleId != null){
      final dataState = await findSiteVisitScheduleUseCase(params: siteVisitScheduleId);

      if(dataState.data != null){
        DetailedSiteVisitScheduleModel detailedSiteVisitScheduleModel = dataState.data!;

        yield SuccessFindSiteVisitScheduleState(detailedSiteVisitScheduleModel);
      } else {
        yield const ErrorFindSiteVisitScheduleState();
      }
    } else {
      yield const ErrorFindSiteVisitScheduleState();
    }
  }
}
