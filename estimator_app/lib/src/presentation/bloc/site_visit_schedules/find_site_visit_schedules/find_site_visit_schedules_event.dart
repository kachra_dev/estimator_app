part of 'find_site_visit_schedules_bloc.dart';

abstract class FindSiteVisitSchedulesEvent extends Equatable {
  const FindSiteVisitSchedulesEvent();

  @override
  List<Object?> get props => [];
}

class FindSiteScheduleEvent extends FindSiteVisitSchedulesEvent{
  final String? siteVisitScheduleId;
  const FindSiteScheduleEvent(this.siteVisitScheduleId);
}