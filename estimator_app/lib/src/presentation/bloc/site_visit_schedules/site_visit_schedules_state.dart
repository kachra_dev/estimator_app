part of 'site_visit_schedules_bloc.dart';

abstract class SiteVisitSchedulesState extends Equatable {
  final List<SiteVisitSchedulesModel>? siteVisitSchedulesList;
  const SiteVisitSchedulesState({this.siteVisitSchedulesList});

  @override
  List<Object?> get props => [];
}

class SiteVisitSchedulesInitial extends SiteVisitSchedulesState {
  const SiteVisitSchedulesInitial();
}

class LoadingSiteVisitSchedulesState extends SiteVisitSchedulesState{
  const LoadingSiteVisitSchedulesState();
}

class SuccessSiteVisitSchedulesState extends SiteVisitSchedulesState{
  final List<SiteVisitSchedulesModel>? siteVisitSchedules;
  const SuccessSiteVisitSchedulesState(this.siteVisitSchedules) : super(siteVisitSchedulesList: siteVisitSchedules);
}

class ErrorSiteVisitSchedulesState extends SiteVisitSchedulesState {
  const ErrorSiteVisitSchedulesState();
}