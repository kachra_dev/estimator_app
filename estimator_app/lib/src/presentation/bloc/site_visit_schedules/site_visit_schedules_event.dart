part of 'site_visit_schedules_bloc.dart';

abstract class SiteVisitSchedulesEvent extends Equatable {
  const SiteVisitSchedulesEvent();

  @override
  List<Object?> get props => [];
}

class GetSiteVisitSchedulesEvent extends SiteVisitSchedulesEvent{
  const GetSiteVisitSchedulesEvent();
}
