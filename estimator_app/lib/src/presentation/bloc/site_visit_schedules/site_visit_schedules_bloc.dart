import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:estimator_app/src/data/models/site_visit_schedules/site_visit_schedules_model.dart';
import 'package:estimator_app/src/domain/usecases/site_visit_usecase.dart';

part 'site_visit_schedules_event.dart';
part 'site_visit_schedules_state.dart';

class SiteVisitSchedulesBloc extends Bloc<SiteVisitSchedulesEvent, SiteVisitSchedulesState> {
  final SiteVisitScheduleUseCase siteVisitScheduleUseCase;
  SiteVisitSchedulesBloc(this.siteVisitScheduleUseCase) : super(const SiteVisitSchedulesInitial());

  @override
  Stream<SiteVisitSchedulesState> mapEventToState(SiteVisitSchedulesEvent event) async* {
    if (event is GetSiteVisitSchedulesEvent) {
      yield* _getAllSiteVisitSchedules();
    }
  }

  Stream<SiteVisitSchedulesState> _getAllSiteVisitSchedules() async* {
    yield const LoadingSiteVisitSchedulesState();

    final dataState = await siteVisitScheduleUseCase();

    if(dataState.data != null){
      List<SiteVisitSchedulesModel> siteVisitSchedules = dataState.data!;

      yield SuccessSiteVisitSchedulesState(siteVisitSchedules);
    } else {
      yield const ErrorSiteVisitSchedulesState();
    }
  }
}
