part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object?> get props => [];

  @override
  bool? get stringify => true;
}

class LoginAuthEvent extends AuthEvent {
  final AuthParams? authParams;
  final BuildContext context;
  const LoginAuthEvent(this.authParams, this.context);
}

class LoginAuthGoogleEvent extends AuthEvent {
  final String? params;
  final BuildContext context;
  const LoginAuthGoogleEvent(this.params, this.context);
}

class GetStoredAuthEvent extends AuthEvent {
  const GetStoredAuthEvent();
}

class RegisterAuthEvent extends AuthEvent {
  final RegisterAuthParams registerAuthParams;
  final BuildContext context;
  const RegisterAuthEvent(this.registerAuthParams, this.context);
}

class RegisterAuthGoogleEvent extends AuthEvent {
  final RegisterGoogleAuthParams registerGoogleAuthParams;
  final BuildContext context;
  const RegisterAuthGoogleEvent(this.registerGoogleAuthParams, this.context);
}

class LogOutEvent extends AuthEvent{
  const LogOutEvent();
}