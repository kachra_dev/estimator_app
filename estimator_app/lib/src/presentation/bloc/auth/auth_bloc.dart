import 'dart:async';
import 'dart:convert';

import 'package:estimator_app/src/core/params/auth_params.dart';
import 'package:estimator_app/src/core/utils/helper/driver_registration_summary.dart';
import 'package:estimator_app/src/core/utils/hive/add_hive.dart';
import 'package:estimator_app/src/core/utils/log_out/log_out.dart';
import 'package:estimator_app/src/core/utils/messages/error_message.dart';
import 'package:estimator_app/src/core/utils/messages/success_message.dart';
import 'package:estimator_app/src/core/utils/shared_preferences/get_preference.dart';
import 'package:estimator_app/src/core/utils/shared_preferences/store_preference.dart';
import 'package:estimator_app/src/data/models/auth/driver_model.dart';
import 'package:estimator_app/src/data/models/hive/driver_hive.dart';
import 'package:estimator_app/src/data/models/hive/profile_hive.dart';
import 'package:estimator_app/src/data/models/profile/profile_model.dart';
import 'package:estimator_app/src/domain/usecases/user_usecase.dart';
import 'package:estimator_app/translations/locale_keys.g.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logger/logger.dart';
import 'package:easy_localization/easy_localization.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final UserLoginUseCase userLoginUseCase;
  final RegisterDriverUseCase registerDriverUseCase;
  final UserLoginGoogleUseCase userLoginGoogleUseCase;
  final RegisterDriverGoogleUseCase registerDriverGoogleUseCase;

  AuthBloc(this.userLoginUseCase, this.registerDriverUseCase,
      this.userLoginGoogleUseCase, this.registerDriverGoogleUseCase)
      : super(const AuthLoadingState());

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is LoginAuthEvent) {
      yield* _loginAuth(event.authParams, event.context);
    }

    if (event is LoginAuthGoogleEvent) {
      yield* _loginAuthGoogle(event.params, event.context);
    }

    if (event is GetStoredAuthEvent) {
      yield* _getStoredAuthEvent();
    }

    if (event is RegisterAuthEvent) {
      yield* _registerAuth(event.registerAuthParams, event.context);
    }

    if (event is RegisterAuthGoogleEvent) {
      yield* _registerAuthGoogle(event.registerGoogleAuthParams, event.context);
    }

    if (event is LogOutEvent) {
      yield* _logOut();
    }
  }

  Stream<AuthState> _loginAuth(
      AuthParams? authParams, BuildContext context) async* {
    yield const AuthLoadingState();

    var logger = Logger();

    final dataState = await userLoginUseCase(params: authParams);

    logger.i(dataState);
    if (authParams != null) {
      if (dataState.data != null) {
        String? role = dataState.data!.user!.role;

        if (role != 'Estimator') {
          showDialog(
              context: context,
              builder: (context) => ErrorMessage(
                  title: LocaleKeys.error.tr(),
                  content: 'User is not an Estimator.',
                  onPressedFunction: () => Navigator.pop(context)));
          yield const UnauthenticatedState();
        } else {
          String accessToken = dataState.data!.accessToken!;
          ProfileModel? profile = dataState.data!.user;
          DriverModel? driver = dataState.data!.driver;

          ///add accessToken
          StorePreference().storeAccessToken(accessToken);

          ///add user
          Map<String, dynamic> profileJson = profile!.toJson();
          // Map<String, dynamic> driverJson = driver!.toJson();

          ProfileHive profileHive = ProfileHive.fromJson(profileJson);
          // DriverHive driverHive = DriverHive.fromJson(driverJson);

          showDialog(
              context: context,
              builder: (context) => SuccessMessage(
                  title: LocaleKeys.success.tr(),
                  content:
                      'Successfully logged in. \nEmail: ${profileHive.email} \nPassword: **********',
                  onPressedFunction: () {
                    Navigator.of(context, rootNavigator: true).pop();
                  }));

          AddHive().addUser(profileHive);

          yield const AuthenticatedState();
        }
      } else {
        //TODO: GET MESSAGES THROUGH SERVER
        Map<String, dynamic> mapError =
            dataState.error!.response!.data! as Map<String, dynamic>;
        List<dynamic> errorValues =
            mapError.values.toList().map((e) => e.toString()).toList();

        logger.i(errorValues);

        Widget errorMessage = TextButton(
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (context) => ErrorMessage(
                      title: 'Error:',
                      content: errorValues.toString(),
                      onPressedFunction: () => Navigator.pop(context)));
            },
            child: const Text('View error message.'));

        showDialog(
            context: context,
            builder: (context) => ErrorMessage(
                title: LocaleKeys.error.tr(),
                content: LocaleKeys.error_logging_in.tr(),
                errorMessage: errorMessage,
                onPressedFunction: () => Navigator.pop(context)));

        yield const UnauthenticatedState();
      }
    } else {
      logger.i("error");
      showDialog(
          context: context,
          builder: (context) => ErrorMessage(
              title: LocaleKeys.error.tr(),
              content: LocaleKeys
                  .error_logging_in_please_check_your_internet_connection
                  .tr(),
              onPressedFunction: () => Navigator.pop(context)));
      yield const UnauthenticatedState();
    }
  }

  Stream<AuthState> _loginAuthGoogle(
      String? params, BuildContext context) async* {
    yield const AuthLoadingState();

    var logger = Logger();

    final dataState = await userLoginGoogleUseCase(params: params);

    logger.i(dataState);
    if (params != null) {
      if (dataState.data != null) {
        String? role = dataState.data!.user!.role;

        if (role != 'Estimator') {
          showDialog(
              context: context,
              builder: (context) => ErrorMessage(
                  title: LocaleKeys.error.tr(),
                  content: 'User is not an Estimator.',
                  onPressedFunction: () => Navigator.pop(context)));
          yield const UnauthenticatedState();
        } else {
          String accessToken = dataState.data!.accessToken!;
          ProfileModel? profile = dataState.data!.user;
          DriverModel? driver = dataState.data!.driver;

          ///add accessToken
          StorePreference().storeAccessToken(accessToken);

          ///add user
          Map<String, dynamic> profileJson = profile!.toJson();
          Map<String, dynamic> driverJson = driver!.toJson();

          ProfileHive profileHive = ProfileHive.fromJson(profileJson);
          DriverHive driverHive = DriverHive.fromJson(driverJson);

          showDialog(
              context: context,
              builder: (context) => SuccessMessage(
                  title: LocaleKeys.success.tr(),
                  content:
                      '${LocaleKeys.successfully_logged_in.tr()} \n${LocaleKeys.email.tr()}: ${profileHive.email} \n${LocaleKeys.password.tr()}: **********',
                  onPressedFunction: () {
                    Navigator.of(context, rootNavigator: true).pop();
                  }));

          AddHive().addUser(profileHive);

          yield const AuthenticatedState();
        }
      } else {
        //TODO: GET MESSAGES THROUGH SERVER
        Map<String, dynamic> mapError =
            dataState.error!.response!.data! as Map<String, dynamic>;
        List<dynamic> errorValues =
            mapError.values.toList().map((e) => e.toString()).toList();

        logger.i(errorValues);

        Widget errorMessage = TextButton(
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (context) => ErrorMessage(
                      title: 'Error:',
                      content: errorValues.toString(),
                      onPressedFunction: () => Navigator.pop(context)));
            },
            child: const Text('View error message.'));

        showDialog(
            context: context,
            builder: (context) => ErrorMessage(
                title: LocaleKeys.error.tr(),
                content: LocaleKeys.error_logging_in.tr(),
                errorMessage: errorMessage,
                onPressedFunction: () => Navigator.pop(context)));

        yield const UnauthenticatedState();
      }
    } else {
      logger.i("error");
      showDialog(
          context: context,
          builder: (context) => ErrorMessage(
              title: LocaleKeys.error.tr(),
              content: LocaleKeys
                  .error_logging_in_please_check_your_internet_connection
                  .tr(),
              onPressedFunction: () => Navigator.pop(context)));
      yield const UnauthenticatedState();
    }
  }

  Stream<AuthState> _getStoredAuthEvent() async* {
    String? accessToken = await GetPreference().getAccessToken();

    if (accessToken != null) {
      yield const AuthenticatedState();
    } else {
      yield const UnauthenticatedState();
    }
  }

  Stream<AuthState> _registerAuth(
      RegisterAuthParams? registerParams, BuildContext context) async* {
    yield const AuthLoadingState();

    var logger = Logger();

    final dataState = await registerDriverUseCase(params: registerParams);

    logger.i(dataState);
    if (registerParams != null && dataState.data != null) {
      showDialog(
          context: context,
          builder: (context) => SuccessMessage(
              title: 'Success!',
              content: 'Please log in using your credentials.',
              onPressedFunction: () {
                ClearDriverRegistration().clearDriverRegistration();

                Navigator.pop(context);
                Navigator.pop(context);
              }));

      yield const UnauthenticatedState();
    } else {
      logger.i("error");
      showDialog(
          context: context,
          builder: (context) => ErrorMessage(
              title: LocaleKeys.error.tr(),
              content: LocaleKeys
                  .error_logging_in_please_check_your_internet_connection
                  .tr(),
              onPressedFunction: () => Navigator.pop(context)));

      yield const UnauthenticatedState();
    }
  }

  Stream<AuthState> _registerAuthGoogle(
      RegisterGoogleAuthParams? registerGoogleAuthParams,
      BuildContext context) async* {
    yield const AuthLoadingState();

    var logger = Logger();

    final dataState =
        await registerDriverGoogleUseCase(params: registerGoogleAuthParams);

    logger.i(dataState);
    if (registerGoogleAuthParams != null && dataState.data != null) {
      showDialog(
          context: context,
          builder: (context) => SuccessMessage(
              title: 'Success!',
              content: 'Please log in using your credentials.',
              onPressedFunction: () {
                ClearDriverRegistration().clearDriverRegistration();

                Navigator.pop(context);
                Navigator.pop(context);
              }));

      yield const UnauthenticatedState();
    } else {
      logger.i("error");
      showDialog(
          context: context,
          builder: (context) => ErrorMessage(
              title: LocaleKeys.error.tr(),
              content: LocaleKeys
                  .error_logging_in_please_check_your_internet_connection
                  .tr(),
              onPressedFunction: () => Navigator.pop(context)));

      yield const UnauthenticatedState();
    }
  }

  Stream<AuthState> _logOut() async* {
    await LogOut().logOut();
    yield const UnauthenticatedState();
  }
}
