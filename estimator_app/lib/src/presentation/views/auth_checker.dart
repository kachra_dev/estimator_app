import 'package:estimator_app/src/presentation/bloc/auth/auth_bloc.dart';
import 'package:estimator_app/src/presentation/views/authenticated/dashboard/dashboard.dart';
import 'package:estimator_app/src/presentation/views/unauthenticated/login/login.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:logger/logger.dart';

class AuthChecker extends StatefulWidget {
  const AuthChecker({Key? key}) : super(key: key);

  @override
  _AuthCheckerState createState() => _AuthCheckerState();
}

class _AuthCheckerState extends State<AuthChecker> {
  Future<void> initApp() async {
    await Hive.openBox('userProfile');
    await Hive.openBox('address');
    await Hive.openBox('driverProfile');
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: initApp(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
              if (state is AuthenticatedState) {
                return const Dashboard();
              } else {
                return const Login();
              }
            });
          } else {
            return const Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
        });
  }
}
