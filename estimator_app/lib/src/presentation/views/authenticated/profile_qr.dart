import 'package:estimator_app/src/core/utils/hive/get_hive.dart';
import 'package:estimator_app/src/data/models/hive/profile_hive.dart';
import 'package:estimator_app/translations/locale_keys.g.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:easy_localization/easy_localization.dart';

class ProfileQR extends StatefulWidget {
  const ProfileQR({Key? key}) : super(key: key);

  @override
  _ProfileQRState createState() => _ProfileQRState();
}

class _ProfileQRState extends State<ProfileQR> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: GetHive().getCurrentUserProfile(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            ProfileHive profile = snapshot.data as ProfileHive;

            return Scaffold(
              appBar: AppBar(
                title: const Text('Your Profile',
                    style: TextStyle(color: Colors.black)),
                iconTheme: const IconThemeData(color: Colors.black),
              ),
              body: Center(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      QrImage(
                        data: profile.id!,
                        size: 200.0,
                      ),
                      const SizedBox(height: 10.0),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(LocaleKeys.full_name.tr()),
                          const SizedBox(width: 5.0),
                          Text(profile.name!)
                        ],
                      ),
                      const SizedBox(height: 10.0),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(LocaleKeys.email.tr()),
                          const SizedBox(width: 5.0),
                          Text(profile.email!)
                        ],
                      ),
                      const SizedBox(height: 10.0),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(LocaleKeys.phone_number.tr()),
                          const SizedBox(width: 5.0),
                          Text(profile.phoneNumber!)
                        ],
                      ),
                      const SizedBox(height: 10.0),
                      Text(LocaleKeys.address.tr()),
                      const SizedBox(height: 5.0),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(LocaleKeys.full_name.tr()),
                          const SizedBox(width: 5.0),
                          Text(profile.address!.name ?? 'N/A')
                        ],
                      ),
                      const SizedBox(height: 5.0),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(LocaleKeys.street.tr()),
                          const SizedBox(width: 5.0),
                          Text(profile.address!.street ?? 'N/A')
                        ],
                      ),
                      const SizedBox(height: 5.0),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(LocaleKeys.locality.tr()),
                          const SizedBox(width: 5.0),
                          Text(profile.address!.locality ?? 'N/A')
                        ],
                      ),
                      const SizedBox(height: 10.0),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(LocaleKeys.country.tr()),
                          const SizedBox(width: 5.0),
                          Text(profile.country!)
                        ],
                      )
                    ],
                  ),
                ),
              ),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }
}
