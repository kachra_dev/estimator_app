// import 'dart:async';
//
// import 'package:flutter/material.dart';
// import 'package:flutter_pdfview/flutter_pdfview.dart';
//
// class LegalDocs extends StatefulWidget {
//   final String pathPDF;
//   const LegalDocs({Key? key, required this.pathPDF}) : super(key: key);
//
//   @override
//   _LegalDocsState createState() => _LegalDocsState();
// }
//
// class _LegalDocsState extends State<LegalDocs> {
//   final Completer<PDFViewController> _controller =
//   Completer<PDFViewController>();
//   int? pages = 0;
//   int? currentPage = 0;
//   bool isReady = false;
//   String errorMessage = '';
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         iconTheme: const IconThemeData(color: Colors.black),
//       ),
//       body: Center(
//         child: PDFView(
//           filePath: widget.pathPDF,
//           enableSwipe: true,
//           swipeHorizontal: true,
//           autoSpacing: true,
//           pageFling: true,
//           pageSnap: true,
//           defaultPage: currentPage!,
//           fitPolicy: FitPolicy.BOTH,
//           preventLinkNavigation: false, // if set to true the link is handled in flutter
//           onRender: (_pages) {
//             setState(() {
//               pages = _pages;
//               isReady = true;
//             });
//           },
//           onError: (error) {
//             setState(() {
//               errorMessage = error.toString();
//             });
//           },
//           onPageError: (page, error) {
//             setState(() {
//               errorMessage = '$page: ${error.toString()}';
//             });
//           },
//           onViewCreated: (PDFViewController pdfViewController) {
//             _controller.complete(pdfViewController);
//           },
//           onLinkHandler: (String? uri) {
//           },
//           onPageChanged: (int? page, int? total) {
//             setState(() {
//               currentPage = page;
//             });
//           },
//         ),
//       ),
//     );
//   }
// }
