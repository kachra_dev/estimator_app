import 'dart:convert';

import 'package:estimator_app/src/core/utils/helper/notification_api.dart';
import 'package:estimator_app/src/core/utils/hive/get_hive.dart';
import 'package:estimator_app/src/data/models/hive/driver_hive.dart';
import 'package:estimator_app/src/data/models/hive/profile_hive.dart';
import 'package:estimator_app/src/presentation/bloc/auth/auth_bloc.dart';
import 'package:estimator_app/src/presentation/bloc/booking/booking_bloc.dart';
import 'package:estimator_app/src/presentation/bloc/site_visit_schedules/site_visit_schedules_bloc.dart';
import 'package:estimator_app/src/presentation/views/authenticated/dashboard/body_dashboard.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logger/logger.dart';
import 'package:pusher_client/pusher_client.dart';

import 'content/booking/preview/booking_preview.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  void initState() {
    super.initState();
    getNotifications();
    NotificationApi().initNotification();
    listenNotification();
  }

  listenNotification() =>
      NotificationApi.onNotifications.stream.listen((String? payload) {
        var logger = Logger();
        logger.i('pass here');
        logger.i(payload);

        if (payload != null) {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => BookingPreview(payload: payload)));
        }
      });

  PusherClient? pusher;
  Channel? channel;

  Future<void> getBookings() async {
    ///get all assigned bookings
    final bookingBloc = BlocProvider.of<BookingBloc>(context);
    bookingBloc.add(const GetAllBookingsEvent());

    ///get all schedules
    final siteVisitSchedulesBloc = BlocProvider.of<SiteVisitSchedulesBloc>(context);
    siteVisitSchedulesBloc.add(const GetSiteVisitSchedulesEvent());
  }

  Future<void> getNotifications() async {
    final authBloc = BlocProvider.of<AuthBloc>(context);

    if (authBloc.state is AuthenticatedState) {
      var logger = Logger();

      ProfileHive profile = await GetHive().getCurrentUserProfile();
      String currentProfileId = profile.id.toString();

      pusher = PusherClient("5f6d5a6407197f144d57", PusherOptions(cluster: 'ap2'));

      channel = pusher!.subscribe("kachra-channel");

      channel!.bind('kachra', (PusherEvent? event) {
        logger.i(event!.data.toString());

        Map<String, dynamic> jsonNotification =
            jsonDecode(event.data.toString());
        Map<String, dynamic> jsonNotificationMessage =
            jsonNotification['message'];
        String? userId = jsonNotification['userID'];

        logger.i(jsonNotification);
        logger.i(jsonNotificationMessage['asset']);

        var outputFormat = DateFormat('MMMM dd, yyyy | hh:mm a');
        DateTime resDateTime = DateTime.now();

        String dateTime = outputFormat.format(resDateTime).toString();

        if (userId == 'Estimator') {
          FlutterLocalNotificationModel flutterLocalNotificationModel =
              FlutterLocalNotificationModel(
            title: 'Hi! A new booking assigned to you.',
            body: dateTime,
            asset: jsonNotificationMessage['asset'],
            payload: jsonNotificationMessage['id'].toString(),
          );

          NotificationApi().showNotification(flutterLocalNotificationModel);

          getBookings();
        }

        setState(() {});
        logger.i("Status Update Event" + event.data.toString());
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: BodyDashboard(),
    );
  }
}
