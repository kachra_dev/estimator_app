import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import 'content/booking/booking.dart';

class BodyDashboard extends StatefulWidget {
  const BodyDashboard({Key? key}) : super(key: key);

  @override
  _BodyDashboardState createState() => _BodyDashboardState();
}

class _BodyDashboardState extends State<BodyDashboard> {
  @override
  Widget build(BuildContext context) {
    return const Booking();
  }
}
