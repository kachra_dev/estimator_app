import 'package:estimator_app/src/presentation/views/authenticated/dashboard/content/booking/map_booking/maps_sheet.dart';
import 'package:estimator_app/translations/locale_keys.g.dart';
import 'package:flutter/material.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:easy_localization/easy_localization.dart';

class ShowDirections extends StatefulWidget {
  final double? destinationLat;
  final double? destinationLng;
  final String? destinationAddress;
  final double? originLat;
  final double? originLng;
  final String? originAddress;
  const ShowDirections(
      {Key? key,
      this.destinationLat,
      this.destinationLng,
      this.originLat,
      this.originLng,
      this.destinationAddress,
      this.originAddress})
      : super(key: key);

  @override
  _ShowDirectionsState createState() => _ShowDirectionsState();
}

class _ShowDirectionsState extends State<ShowDirections> {
  // double destinationLatitude = 37.759392;
  // double destinationLongitude = -122.5107336;
  // String destinationTitle = 'Ocean Beach';
  //
  // double originLatitude = 37.8078513;
  // double originLongitude = -122.404604;
  // String originTitle = 'Pier 33';

  DirectionsMode directionsMode = DirectionsMode.driving;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Column(
          children: <Widget>[
            FormTitle(LocaleKeys.destination.tr()),
            // TextFormField(
            //   autocorrect: false,
            //   enabled: false,
            //   decoration: const InputDecoration(labelText: 'Destination Latitude'),
            //   initialValue: widget.destinationLat.toString(),
            // ),
            // TextFormField(
            //   autocorrect: false,
            //   enabled: false,
            //   decoration: const InputDecoration(labelText: 'Destination Longitude'),
            //   initialValue: widget.destinationLng.toString(),
            // ),
            TextFormField(
              autocorrect: false,
              enabled: false,
              decoration:
                  InputDecoration(labelText: LocaleKeys.destination.tr()),
              initialValue: widget.destinationAddress.toString(),
            ),
            const SizedBox(height: 30),
            FormTitle(LocaleKeys.origin.tr()),
            // TextFormField(
            //   autocorrect: false,
            //   enabled: false,
            //   decoration: const InputDecoration(labelText: 'Origin Latitude'),
            //   initialValue: widget.originLat.toString(),
            // ),
            // TextFormField(
            //   autocorrect: false,
            //   enabled: false,
            //   decoration: const InputDecoration(labelText: 'Origin Longitude'),
            //   initialValue: widget.originLng.toString(),
            // ),
            TextFormField(
              autocorrect: false,
              enabled: false,
              decoration:
                  InputDecoration(labelText: LocaleKeys.original_address.tr()),
              initialValue: widget.originAddress.toString(),
            ),
            const SizedBox(height: 30),
            FormTitle(LocaleKeys.direction_mode.tr()),
            Container(
              alignment: Alignment.centerLeft,
              child: DropdownButton(
                value: directionsMode,
                onChanged: (newValue) {
                  setState(() {
                    directionsMode = newValue as DirectionsMode;
                  });
                },
                items: DirectionsMode.values.map((directionsMode) {
                  return DropdownMenuItem(
                    value: directionsMode,
                    child: Text(directionsMode.toString()),
                  );
                }).toList(),
              ),
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                MapsSheet.show(
                  context: context,
                  onMapTap: (map) {
                    map.showDirections(
                      destination: Coords(
                        widget.destinationLat!,
                        widget.destinationLng!,
                      ),
                      destinationTitle: widget.destinationAddress,
                      origin: Coords(widget.originLat!, widget.originLng!),
                      originTitle: widget.originAddress!,
                      directionsMode: directionsMode,
                    );
                  },
                );
              },
              child: Text(LocaleKeys.show_map.tr()),
            )
          ],
        ),
      ),
    );
  }
}

class FormTitle extends StatelessWidget {
  final String title;
  final Widget? trailing;

  FormTitle(this.title, {this.trailing});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 20),
        Row(
          children: [
            Text(
              title,
              style: const TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w600,
                color: Colors.blue,
              ),
            ),
            const Spacer(),
            if (trailing != null) trailing!,
          ],
        ),
      ],
    );
  }
}
