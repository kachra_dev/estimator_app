import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:logger/logger.dart';

const apiKey = "AIzaSyC2VY7-9oxNaoMyxtt5Rd3W6zuAHFeeinE";

class GoogleMapsServices{
  Future<String> getRouteCoordinates(LatLng l1, LatLng l2) async {
    var logger = Logger();
    String url = "https://maps.googleapis.com/maps/api/directions/json?origin=${l1.latitude},${l1.longitude}&destination=${l2.latitude},${l2.longitude}&key=$apiKey";

    logger.i(url);
    http.Response response = await http.get(Uri.parse(url));
    Map values = jsonDecode(response.body);

    return values["routes"][0]["overview_polyline"]["points"];
  }
}