import 'dart:async';

import 'package:estimator_app/main.dart';
import 'package:estimator_app/src/core/utils/helper/text_checker.dart';
import 'package:estimator_app/src/presentation/bloc/auth/auth_bloc.dart';
import 'package:estimator_app/src/presentation/bloc/site_visit_schedules/site_visit_schedules_bloc.dart';
import 'package:estimator_app/src/presentation/views/authenticated/dashboard/content/booking/list_bookings/list_bookings.dart';
import 'package:estimator_app/src/presentation/views/authenticated/dashboard/content/booking/site_visit_schedules/table_calendar_sched.dart';
import 'package:estimator_app/src/presentation/views/authenticated/profile_qr.dart';
import 'package:estimator_app/translations/locale_keys.g.dart';
import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:url_launcher/url_launcher.dart';

class BookingDrawer extends StatefulWidget {
  const BookingDrawer({Key? key}) : super(key: key);

  @override
  State<BookingDrawer> createState() => _BookingDrawerState();
}
class _BookingDrawerState extends State<BookingDrawer> {
  PackageInfo packageInfo =
      PackageInfo(appName: '', packageName: '', version: '', buildNumber: '');

  Future<void> initData() async {
    PackageInfo initPackageInfo = await PackageInfo.fromPlatform();

    setState(() {
      packageInfo = initPackageInfo;
    });
  }

  @override
  void initState() {
    super.initState();
    initData();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
              child:
              Image.asset('assets/img/logo.png', scale: 2.0)),
          ListTile(
            title: Row(
              children: [
                const Icon(Icons.my_library_books_outlined),
                const SizedBox(width: 10.0),
                Text(LocaleKeys.assigned_bookings.tr())
              ],
            ),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const ListBookings()));
            },
          ),
          ListTile(
            title: Row(
              children: [
                const Icon(Icons.calendar_today),
                const SizedBox(width: 10.0),
                Text(LocaleKeys.site_visit_schedule.tr())
              ],
            ),
            onTap: () {
              final siteVisitSchedulesBloc = BlocProvider.of<SiteVisitSchedulesBloc>(context);
              siteVisitSchedulesBloc.add(const GetSiteVisitSchedulesEvent());

              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const TableCalendarSched()));
            },
          ),
          ListTile(
            title: Row(
              children: const [
                Icon(Icons.qr_code),
                SizedBox(width: 10.0),
                Text('Profile QR')
              ],
            ),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const ProfileQR()));
            },
          ),
          Column(
            children: [
              ExpansionTile(
                title: const Text('Languages'),
                leading: const Icon(Icons.language),
                initiallyExpanded: false,
                children: [
                  ListTile(
                    onTap: () async {
                      await MyApp.navigatorKey.currentContext!
                          .setLocale(const Locale('en'));
                      Navigator.popUntil(context,
                              (Route<dynamic> route) => route.isFirst);

                      MyApp.restartApp(context);
                    },
                    leading: const Icon(
                      CupertinoIcons.bookmark_solid,
                      color: Colors.black,
                    ),
                    title: Text(
                      'English',
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ),
                  ListTile(
                    onTap: () async {
                      await MyApp.navigatorKey.currentContext!
                          .setLocale(const Locale('ar'));
                      Navigator.popUntil(context,
                              (Route<dynamic> route) => route.isFirst);

                      MyApp.restartApp(context);
                    },
                    leading: const Icon(
                      CupertinoIcons.bookmark_solid,
                      color: Colors.black,
                    ),
                    title: Text('عربي', style: Theme.of(context).textTheme.subtitle1),
                  ),
                ],
              )
            ],
          ),
          Column(
            children: [
              ExpansionTile(
                title: const Text('Legal Documents'),
                initiallyExpanded: false,
                children: [
                  ListTile(
                    onTap: () async {
                      String url =
                          'https://kchraapp.com/#/terms-and-conditions';
                      if (!await launch(url)) {
                        throw 'Could not launch $url';
                      }
                    },
                    leading: const Icon(
                      CupertinoIcons.doc_text_fill,
                      color: Colors.black,
                    ),
                    title: Text(
                      'Terms and Conditions',
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ),
                  ListTile(
                    onTap: () async {
                      String url =
                          'https://kchraapp.com/#/privacy-policy';
                      if (!await launch(url)) {
                        throw 'Could not launch $url';
                      }
                    },
                    leading: const Icon(
                      CupertinoIcons.doc_text_fill,
                      color: Colors.black,
                    ),
                    title: Text(
                      'Privacy Policy',
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ),
                  ListTile(
                    onTap: () async {
                      String url = 'https://kchraapp.com/#/disclaimer';
                      if (!await launch(url)) {
                        throw 'Could not launch $url';
                      }
                    },
                    leading: const Icon(
                      CupertinoIcons.doc_text_fill,
                      color: Colors.black,
                    ),
                    title: Text(
                      'Disclaimer',
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ),
                  ListTile(
                    onTap: () async {
                      String url =
                          'https://kchraapp.com/#/copyright-policy';
                      if (!await launch(url)) {
                        throw 'Could not launch $url';
                      }
                    },
                    leading: const Icon(
                      CupertinoIcons.doc_text_fill,
                      color: Colors.black,
                    ),
                    title: Text(
                      'Copyright Policy',
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ),
                ],
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
                style: ButtonStyle(
                    backgroundColor:
                    MaterialStateProperty.all(Colors.red)),
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                        title: Text(
                            '${LocaleKeys.confirm.tr()} $confirmEmoji'),
                        content: Text(LocaleKeys
                            .are_you_sure_you_want_to_log_out
                            .tr()),
                        actions: [
                          TextButton(
                              onPressed: () {
                                final authBloc =
                                BlocProvider.of<AuthBloc>(
                                    context);
                                authBloc.add(const LogOutEvent());

                                Navigator.pop(context);
                              },
                              child: Text(
                                  '${LocaleKeys.log_out.tr()} $yesEmoji')),
                          TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text(
                                  '${LocaleKeys.cancel.tr()} $noEmoji')),
                        ],
                      ));
                },
                child: Text(LocaleKeys.log_out.tr())),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Text("Version: ${packageInfo.version}"),
          ),
        ],
      ),
    );
  }
}
