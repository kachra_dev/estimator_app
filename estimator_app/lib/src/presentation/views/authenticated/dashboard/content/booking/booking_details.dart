import 'package:estimator_app/src/data/models/booking/estimator_booking_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:logger/logger.dart';
import 'package:sizer/sizer.dart';
import 'package:video_player/video_player.dart';
import 'package:chewie/chewie.dart';

class BookingDetails extends StatefulWidget {
  final EstimatorBookingModel booking;
  const BookingDetails({Key? key, required this.booking}) : super(key: key);

  @override
  _BookingDetailsState createState() => _BookingDetailsState();
}

class _BookingDetailsState extends State<BookingDetails> {
  @override
  Widget build(BuildContext context) {
    String? estimatedAmount = widget.booking.estimatedAmount != null
        ? widget.booking.estimatedAmount.toString()
        : 'To be verified';

    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: SingleChildScrollView(
        child: SizedBox(
          height: 100.0.h,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                children: [
                  IconButton(
                      onPressed: () async {
                        Navigator.of(context, rootNavigator: true).pop();
                      },
                      icon: const Icon(Icons.arrow_back))
                ],
              ),
              const Spacer(),
              VideoCreator(url: widget.booking.asset),
              const SizedBox(height: 30),
              const Text('Estimated Amount:'),
              const SizedBox(height: 5),
              Text(
                estimatedAmount.toString() + ' QAR',
                style: TextStyle(
                    fontSize: 17.0.sp,
                    fontWeight: FontWeight.w600,
                    color: Colors.green),
              ),
              const SizedBox(height: 30),
              const Text('Driver Assigned:'),
              const SizedBox(height: 30),
              widget.booking.estimatedAmount != null
                  ? Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                            onPressed: () {
                              // var logger = Logger();
                              // logger.i('print');
                              // Navigator.of(context, rootNavigator: true).push(MaterialPageRoute(builder: (context) => const MakePayment()));
                              // Navigator.push(context, MaterialPageRoute(builder: (context) => const MakePayment()));
                            },
                            child: const Text('Approve')),
                        const SizedBox(width: 30),
                        OutlinedButton(
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all(Colors.red)),
                            onPressed: () {},
                            child: const Text('Disapprove',
                                style: TextStyle(color: Colors.white)))
                      ],
                    )
                  : const SizedBox(height: 0),
              const SizedBox(height: 30),
            ],
          ),
        ),
      ),
    );
  }
}

class VideoCreator extends StatefulWidget {
  final String? url;
  const VideoCreator({Key? key, this.url}) : super(key: key);

  @override
  _VideoCreatorState createState() => _VideoCreatorState();
}

class _VideoCreatorState extends State<VideoCreator> {
  VideoPlayerController? _controller;
  ChewieController? chewieController;

  @override
  Widget build(BuildContext context) {
    _controller = VideoPlayerController.network(widget.url!);

    chewieController = ChewieController(
        videoPlayerController: _controller!,
        looping: true,
        autoPlay: true,
        showControls: true,
        allowFullScreen: false,
        deviceOrientationsOnEnterFullScreen: [
          DeviceOrientation.landscapeLeft,
          DeviceOrientation.landscapeRight
        ]);

    return AspectRatio(
        aspectRatio: _controller!.value.aspectRatio,
        child: Chewie(
          controller: chewieController!,
        ));
  }

  @override
  void dispose() {
    _controller!.dispose();
    chewieController!.dispose();
    super.dispose();
  }
}
