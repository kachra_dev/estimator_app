// import 'dart:async';
//
// import 'package:estimator_app/src/config/themes/app_theme.dart';
// import 'package:estimator_app/src/presentation/views/authenticated/dashboard/content/booking/map_request.dart';
// import 'package:flutter/material.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:location/location.dart';
//
// class BodyBooking extends StatefulWidget {
//   const BodyBooking({Key? key}) : super(key: key);
//
//   @override
//   _BodyBookingState createState() => _BodyBookingState();
// }
//
// class _BodyBookingState extends State<BodyBooking> {
//   bool loading = true;
//   final Set<Marker> _markers = {};
//   final Set<Polyline> _polyLines = {};
//   final GoogleMapsServices _googleMapsServices = GoogleMapsServices();
//
//   Set<Polyline> get polyLines => _polyLines;
//
//   final Completer<GoogleMapController> _controller = Completer();
//
//   static const LatLng _center = LatLng(45.521563, -122.677433);
//
//   void _onMapCreated(GoogleMapController controller) {
//     _controller.complete(controller);
//   }
//
//   LatLng latLng = const LatLng(6.1431365, 125.1694449);
//
//   late LocationData currentLocation;
//
//   void _onAddMarkerButtonPressed() {
//     setState(() {
//       _markers.add(Marker(
//         markerId: const MarkerId("111"),
//         position: latLng,
//         icon: BitmapDescriptor.defaultMarker,
//       ));
//     });
//   }
//
//   @override
//   void initState() {
//     // getLocation();
//     super.initState();
//   }
//
//   void onCameraMove(CameraPosition position) {
//     latLng = position.target;
//   }
//
//   List<LatLng> _convertToLatLng(List points) {
//     List<LatLng> result = <LatLng>[];
//     for (int i = 0; i < points.length; i++) {
//       if (i % 2 != 0) {
//         result.add(LatLng(points[i - 1], points[i]));
//       }
//     }
//     return result;
//   }
//
//   void sendRequest() async {
//     LatLng destination = const LatLng(20.008751, 73.780037);
//     String route = await _googleMapsServices.getRouteCoordinates(latLng, destination);
//     createRoute(route);
//     _addMarker(destination, "KTHM Collage");
//   }
//
//   void createRoute(String encondedPoly) {
//     _polyLines.add(Polyline(
//         polylineId: PolylineId(latLng.toString()),
//         width: 4,
//         points: _convertToLatLng(_decodePoly(encondedPoly)),
//         color: Colors.red));
//   }
//
//   void _addMarker(LatLng location, String address) {
//     _markers.add(Marker(
//         markerId: const MarkerId("112"),
//         position: location,
//         infoWindow: InfoWindow(title: address, snippet: "go here"),
//         icon: BitmapDescriptor.defaultMarker));
//   }
//
//   List _decodePoly(String poly) {
//     var list = poly.codeUnits;
//     var lList = [];
//     int index = 0;
//     int len = poly.length;
//     int c = 0;
//     do {
//       var shift = 0;
//       int result = 0;
//
//       do {
//         c = list[index] - 63;
//         result |= (c & 0x1F) << (shift * 5);
//         index++;
//         shift++;
//       } while (c >= 32);
//       if (result & 1 == 1) {
//         result = ~result;
//       }
//       var result1 = (result >> 1) * 0.00001;
//       lList.add(result1);
//     } while (index < len);
//
//     for (var i = 2; i < lList.length; i++) {
//       lList[i] += lList[i - 2];
//     }
//
//     return lList;
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('Maps Sample App'),
//         backgroundColor: AppTheme.light.accentColor,
//       ),
//       body: GoogleMap(
//         polylines: polyLines,
//         markers: _markers,
//         mapType: MapType.normal,
//         initialCameraPosition: CameraPosition(
//           target: latLng,
//           zoom: 14.4746,
//         ),
//         onCameraMove: onCameraMove,
//         onMapCreated: (GoogleMapController controller) {
//           _controller.complete(controller);
//         },
//       ),
//       floatingActionButton: FloatingActionButton.extended(
//         onPressed: () {
//           sendRequest();
//         },
//         label: const Text('Destination'),
//         icon: const Icon(Icons.directions_boat),
//       ),
//     );
//   }
// }

import 'dart:collection';

import 'package:estimator_app/src/config/themes/app_theme.dart';
import 'package:estimator_app/src/core/utils/helper/text_checker.dart';
import 'package:estimator_app/src/core/utils/hive/get_hive.dart';
import 'package:estimator_app/src/core/utils/table_calendar_utils.dart';
import 'package:estimator_app/src/data/models/booking/estimator_booking_model.dart';
import 'package:estimator_app/src/data/models/hive/profile_hive.dart';
import 'package:estimator_app/src/data/models/site_visit_schedules/site_visit_schedules_model.dart';
import 'package:estimator_app/src/presentation/bloc/booking/booking_bloc.dart';
import 'package:estimator_app/src/presentation/bloc/booking/find_booking/find_booking_bloc.dart';
import 'package:estimator_app/src/presentation/bloc/site_visit_schedules/find_site_visit_schedules/find_site_visit_schedules_bloc.dart';
import 'package:estimator_app/src/presentation/bloc/site_visit_schedules/site_visit_schedules_bloc.dart';
import 'package:estimator_app/src/presentation/views/authenticated/dashboard/content/booking/list_bookings/list_bookings.dart';
import 'package:estimator_app/src/presentation/views/authenticated/dashboard/content/booking/preview/booking_preview.dart';
import 'package:estimator_app/src/presentation/views/authenticated/dashboard/content/booking/site_visit_schedules/detailed_site_visit_schedule.dart';
import 'package:estimator_app/translations/locale_keys.g.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:table_calendar/table_calendar.dart';

class BodyBooking extends StatefulWidget {
  const BodyBooking({Key? key}) : super(key: key);

  @override
  _BodyBookingState createState() => _BodyBookingState();
}

class _BodyBookingState extends State<BodyBooking> {
  List<Color> colors = [
    const Color(0xFFFFBD35),
    // const Color(0xFF3FA796),
    // const Color(0xFFA6CF98),
    // const Color(0xFFF05454)
  ];

  Future<void> getBookings() async {
    ///get all assigned bookings
    final bookingBloc = BlocProvider.of<BookingBloc>(context);
    bookingBloc.add(const GetAllBookingsEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFFEFBF3),
      body: FutureBuilder(
          future: GetHive().getCurrentUserProfile(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              ProfileHive profile = snapshot.data as ProfileHive;

              return Padding(
                padding: const EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 20.0, 20.0, 10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              '${LocaleKeys.welcome.tr()}, ${profile.name!.toUpperCase()}!',
                              style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 15.0.sp,
                                  color: AppTheme.light.accentColor),
                            ),
                            Text(
                              LocaleKeys.seize_the_day_deliver_now.tr(),
                              style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 9.0.sp,
                                  color: Colors.grey),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(height: 20),

                      ///Assigned Bookings
                      BlocBuilder<BookingBloc, BookingState>(
                          builder: (context, state) {
                        if (state is SuccessBookingState) {
                          List<EstimatorBookingModel> bookings = state.booking!;

                          return Column(
                            children: [
                              Align(
                                child: Text('${LocaleKeys.assigned_bookings.tr()} (${bookings.length})', style: TextStyle(fontWeight: FontWeight.w700, fontSize: 12.0.sp,), textAlign: TextAlign.left),
                                alignment: Alignment.centerLeft,
                              ),
                              const SizedBox(height: 15),
                              bookings.isEmpty
                                  ? Text(LocaleKeys.no_bookings.tr())
                                  : SizedBox(
                                      height: 80.0.h,
                                      width: 90.0.w,
                                      child: ListView.builder(
                                          scrollDirection: Axis.vertical,
                                          itemCount: bookings.length,
                                          itemBuilder: (context, index) {
                                            var logger = Logger();
                                            EstimatorBookingModel booking =
                                                bookings[index];

                                            var outputFormat = DateFormat('MMM-dd-yyyy hh:mm a');
                                            DateTime resDateTime = DateTime.parse(booking.updatedAt!);

                                            String dateTime = outputFormat
                                                .format(resDateTime)
                                                .toString();

                                            return GestureDetector(
                                              onTap: (){
                                                final findBookingBloc = BlocProvider.of<FindBookingBloc>(context);
                                                findBookingBloc.add(FindBookEvent(booking.id));

                                                Navigator.push(context, MaterialPageRoute(builder: (context) => const BookingPreview()));
                                              },
                                              child: Card(
                                                child: ListTile(
                                                  title: booking.bookingDetails != null ? Text(TextChecker().checkText(booking.bookingDetails!)) : Text('${TextChecker().checkText(booking.user!.name!)}\'s booking'),
                                                  subtitle: Text(dateTime),
                                                  leading: SizedBox(
                                                    height: 30.0,
                                                    child: Image.asset('assets/img/launcher.png')
                                                  ),
                                                  trailing: const Icon(Icons.view_headline, color: Colors.black),
                                                ),
                                              ),
                                            );
                                          }),
                                    ),
                            ],
                          );
                        } else if (state is ErrorBookingState) {
                          return const Center(
                            child: Text('Error fetching bookings'),
                          );
                        } else {
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        }
                      }),

                      const SizedBox(height: 50),
                    ],
                  ),
                ),
              );
            } else {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
          }),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () => getBookings(),
        icon: const Icon(CupertinoIcons.arrow_2_circlepath),
        label: Text(LocaleKeys.sync_bookings.tr()),
      ),
    );
  }
}
