import 'package:estimator_app/src/data/models/booking/estimator_booking_model.dart';
import 'package:estimator_app/src/presentation/bloc/booking/find_booking/find_booking_bloc.dart';
import 'package:estimator_app/src/presentation/views/authenticated/dashboard/content/booking/map_booking/map_booking.dart';
import 'package:estimator_app/src/presentation/views/authenticated/dashboard/content/booking/preview/booking_preview.dart';
import 'package:estimator_app/translations/locale_keys.g.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:easy_localization/easy_localization.dart';

class BookingModalSheet extends StatelessWidget {
  final EstimatorBookingModel driverSummaryBookingModel;
  final String page;
  const BookingModalSheet(
      {Key? key, required this.driverSummaryBookingModel, required this.page})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
        child: SafeArea(
      top: false,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            title: Center(child: Text('What do you want to do?')),
          ),
          ListTile(
            title: Text(LocaleKeys.destination_location_in_maps_waze.tr()),
            leading: const Icon(Icons.map),
            onTap: () async {
              bool serviceEnabled;
              LocationPermission permission;

              serviceEnabled = await Geolocator.isLocationServiceEnabled();
              if (!serviceEnabled) {
                return Future.error('Location services are disabled.');
              }

              permission = await Geolocator.checkPermission();
              if (permission == LocationPermission.denied) {
                permission = await Geolocator.requestPermission();
                if (permission == LocationPermission.denied) {
                  return Future.error('Location permissions are denied');
                }
              }

              if (permission == LocationPermission.deniedForever) {
                // Permissions are denied forever, handle appropriately.
                return Future.error(
                    'Location permissions are permanently denied, we cannot request permissions.');
              }

              Position location = await Geolocator.getCurrentPosition();
              List<Placemark> p = await placemarkFromCoordinates(
                  location.latitude, location.longitude);
              Placemark place = p[0];

              String address = place.street.toString() +
                  ', ' +
                  place.administrativeArea.toString();

              Navigator.of(context).pop();
            },
          ),
          ListTile(
            title: const Text('View Details'),
            leading: const Icon(Icons.source),
            onTap: () async {
              switch (page) {
                case 'Assigned':
                  final findBookingBloc =
                      BlocProvider.of<FindBookingBloc>(context);
                  findBookingBloc
                      .add(FindBookEvent(driverSummaryBookingModel.id));

                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const BookingPreview()));

                  // if (booking!.assetType == 'Image') {
                  //   Navigator.push(context, MaterialPageRoute(builder: (context) => BookingPreview(booking: booking!, isVideo: false)));
                  // } else {
                  //   Navigator.push(context, MaterialPageRoute(builder: (context) => BookingPreview(booking: booking!, isVideo: true)));
                  // }
                  break;
                case 'Picked-Up':
                  // if (booking!.assetType == 'Image') {
                  //   Navigator.push(context, MaterialPageRoute(builder: (context) => PickedUpBookingPreview(booking: booking!, isVideo: false)));
                  // } else {
                  //   Navigator.push(context, MaterialPageRoute(builder: (context) => PickedUpBookingPreview(booking: booking!, isVideo: true)));
                  // }
                  break;
                case 'Completed':
                  // if (booking!.assetType == 'Image') {
                  //   Navigator.push(context, MaterialPageRoute(builder: (context) => CompleteIncompleteBookingPreview(booking: booking!, isVideo: false)));
                  // } else {
                  //   Navigator.push(context, MaterialPageRoute(builder: (context) => CompleteIncompleteBookingPreview(booking: booking!, isVideo: true)));
                  // }
                  break;
                case 'Incomplete':
                  // if (booking!.assetType == 'Image') {
                  //   Navigator.push(context, MaterialPageRoute(builder: (context) => CompleteIncompleteBookingPreview(booking: booking!, isVideo: false)));
                  // } else {
                  //   Navigator.push(context, MaterialPageRoute(builder: (context) => CompleteIncompleteBookingPreview(booking: booking!, isVideo: true)));
                  // }
                  break;
              }
            },
          ),
        ],
      ),
    ));
  }
}
