import 'package:easy_localization/easy_localization.dart';
import 'package:estimator_app/src/config/themes/app_theme.dart';
import 'package:estimator_app/src/data/models/site_visit_schedules/detailed_site_visit_schedule_model.dart';
import 'package:estimator_app/src/presentation/bloc/site_visit_schedules/find_site_visit_schedules/find_site_visit_schedules_bloc.dart';
import 'package:estimator_app/src/presentation/views/authenticated/dashboard/content/booking/map_booking/map_booking.dart';
import 'package:estimator_app/translations/locale_keys.g.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:sizer/sizer.dart';

class DetailedSiteVisitSchedule extends StatefulWidget {
  const DetailedSiteVisitSchedule({Key? key}) : super(key: key);

  @override
  _DetailedSiteVisitScheduleState createState() =>
      _DetailedSiteVisitScheduleState();
}

class _DetailedSiteVisitScheduleState extends State<DetailedSiteVisitSchedule> {

  int daysBetween(DateTime to) {
    DateTime from = DateTime.now();

    from = DateTime(from.year, from.month, from.day);
    to = DateTime(to.year, to.month, to.day);
    return (to.difference(from).inHours / 24).round();
  }

  int hoursBetween(DateTime to) {
    DateTime from = DateTime.now();
    return (to.difference(from).inHours).round();
  }

  int minutesBetween(DateTime to) {
    DateTime from = DateTime.now();
    return (to.difference(from).inMinutes).round();
  }

  bool isLoadingGoingToClient = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(LocaleKeys.site_visit_information.tr(), style: const TextStyle(color: Colors.white)),
          iconTheme: const IconThemeData(color: Colors.white),
          backgroundColor: AppTheme.light.accentColor,
        ),
        body: Stack(
            children: [
              Container(
                height: 30.0.h,
                decoration: BoxDecoration(
                  color: AppTheme.light.accentColor,
                  image: DecorationImage(image: const AssetImage('assets/img/front.png'), fit: BoxFit.cover, colorFilter: ColorFilter.mode(Colors.grey.withOpacity(0.5), BlendMode.dstATop)),
                ),
              ),
              BlocBuilder<FindSiteVisitSchedulesBloc, FindSiteVisitSchedulesState>(
                builder: (context, state) {
                  if(state is LoadingFindSiteVisitScheduleState){
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  } else if(state is SuccessFindSiteVisitScheduleState){
                    DetailedSiteVisitScheduleModel siteVisitSchedule = state.detailedSiteVisitScheduleModel!;

                    var outputFormat = DateFormat('MMMM dd, yyyy - hh:mm a');
                    DateTime resDateTime = DateTime.parse(siteVisitSchedule.scheduledDate.toString());

                    String dateTime = outputFormat.format(resDateTime).toString();

                    return Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        height: 490,
                        decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(30.0),
                              topRight: Radius.circular(30.0),
                            ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            children: [
                              SizedBox(
                                height: 350,
                                child: SingleChildScrollView(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          SizedBox(
                                            width: 38.0.w,
                                            child: Text(
                                              DateFormat('hh:mm a').format(siteVisitSchedule.scheduledDate!),
                                              style: TextStyle(
                                                color: AppTheme.light.accentColor,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 25.0
                                              ),
                                            ),
                                          ),
                                          const Spacer(),
                                          Expanded(
                                            child: daysBetween(siteVisitSchedule.scheduledDate!) >= 1 ? Text(LocaleKeys.days_left.tr(args: [daysBetween(siteVisitSchedule.scheduledDate!).toString()]),
                                              style: const TextStyle(
                                                color: Colors.blue
                                              ),
                                            ) : hoursBetween(siteVisitSchedule.scheduledDate!) >= 1 ? Text(LocaleKeys.hours_left.tr(args: [hoursBetween(siteVisitSchedule.scheduledDate!).toString()]),
                                              style: const TextStyle(
                                                  color: Colors.orange
                                              ),
                                            ) : minutesBetween(siteVisitSchedule.scheduledDate!) > 0 ? Text(LocaleKeys.minutes_left.tr(args: [minutesBetween(siteVisitSchedule.scheduledDate!).toString()]),
                                              style: const TextStyle(
                                                  color: Colors.red
                                              ),
                                            ) : Text(LocaleKeys.appointment_expired.tr(), style: const TextStyle(color: Colors.red))
                                          )
                                        ],
                                      ),
                                      const SizedBox(height: 30.0),
                                      Text(LocaleKeys.schedule_details.tr(), style: const TextStyle(color: Colors.orange, fontWeight: FontWeight.w500)),
                                      const SizedBox(height: 15.0),
                                      Row(
                                        children: [
                                          const Text('Schedule Date:'),
                                          const SizedBox(width: 15.0),
                                          Expanded(
                                            child: Text(dateTime)
                                          ),
                                        ],
                                      ),
                                      const SizedBox(height: 30.0),
                                      Text(LocaleKeys.users_address.tr(), style: const TextStyle(color: Colors.orange, fontWeight: FontWeight.w500)),
                                      const SizedBox(height: 15.0),
                                      Row(
                                        children: [
                                          const Icon(Icons.location_on_outlined),
                                          Expanded(
                                            child: Text(
                                              siteVisitSchedule.address!.name!
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(height: 30.0),
                                      Text(LocaleKeys.users_information.tr(), style: const TextStyle(color: Colors.orange, fontWeight: FontWeight.w500)),
                                      const SizedBox(height: 15.0),
                                      Row(
                                        children: [
                                          Text(LocaleKeys.full_name.tr()),
                                          const SizedBox(width: 15.0),
                                          Expanded(
                                            child: Text(
                                                siteVisitSchedule.user!.name!
                                            ),
                                          ),
                                        ],
                                      ),
                                      siteVisitSchedule.user!.email != null ? Row(
                                        children: [
                                          Text(LocaleKeys.email.tr()),
                                          const SizedBox(width: 15.0),
                                          Expanded(
                                            child: Text(
                                                siteVisitSchedule.user!.email!
                                            ),
                                          ),
                                        ],
                                      ) : const SizedBox(height: 0),
                                      Row(
                                        children: [
                                          Text(LocaleKeys.phone_number.tr()),
                                          const SizedBox(width: 15.0),
                                          Expanded(
                                            child: Text(
                                                siteVisitSchedule.user!.phoneNumber!
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              const Spacer(),
                              Align(
                                alignment: Alignment.bottomCenter,
                                child: GestureDetector(
                                  onTap: () async {
                                    setState(() {
                                      isLoadingGoingToClient = true;
                                    });
                                    bool serviceEnabled;
                                    LocationPermission permission;

                                    serviceEnabled = await Geolocator.isLocationServiceEnabled();
                                    if (!serviceEnabled) {
                                      return Future.error('Location services are disabled.');
                                    }

                                    permission = await Geolocator.checkPermission();
                                    if (permission == LocationPermission.denied) {
                                      permission = await Geolocator.requestPermission();
                                      if (permission == LocationPermission.denied) {
                                        return Future.error('Location permissions are denied');
                                      }
                                    }

                                    if (permission == LocationPermission.deniedForever) {
                                      // Permissions are denied forever, handle appropriately.
                                      return Future.error(
                                          'Location permissions are permanently denied, we cannot request permissions.');
                                    }

                                    Position location = await Geolocator.getCurrentPosition();
                                    List<Placemark> p = await placemarkFromCoordinates(
                                        location.latitude, location.longitude);
                                    Placemark place = p[0];

                                    String address = place.street.toString() + ', ' + place.administrativeArea.toString();

                                    setState(() {
                                      isLoadingGoingToClient = false;
                                    });

                                    Navigator.of(context).pop();
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => MapBooking(
                                              originLat: location.latitude,
                                              originLng: location.longitude,
                                              originAddress: address,
                                              destinationLat: double.parse(siteVisitSchedule.address!.latitude.toString()),
                                              destinationLng: double.parse(siteVisitSchedule.address!.longitude.toString()),
                                              destinationAddress: siteVisitSchedule.address!.name,
                                            )));
                                  },
                                  child: Container(
                                    height: 80.0,
                                    width: 100.0.w,
                                    decoration: BoxDecoration(
                                      color: Colors.blue,
                                      borderRadius: BorderRadius.circular(20.0)
                                    ),
                                    child: Center(
                                      child: isLoadingGoingToClient ? const CircularProgressIndicator(color: Colors.white) : Text(LocaleKeys.go_to_client.tr(), style: const TextStyle(color: Colors.white, fontSize: 15.0, fontWeight: FontWeight.w600),)
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  } else {
                    return const Center(
                      child: Text('Error finding site visit schedule.', style: TextStyle(color: Colors.white)),
                    );
                  }
                },
              )
            ]
    ));
  }
}
