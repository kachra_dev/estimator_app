import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:chewie/chewie.dart';
import 'package:estimator_app/src/config/themes/app_theme.dart';
import 'package:estimator_app/src/core/params/booking_params.dart';
import 'package:estimator_app/src/core/utils/helper/copy_clipboard.dart';
import 'package:estimator_app/src/core/utils/helper/text_checker.dart';
import 'package:estimator_app/src/data/models/booking/estimator_booking_model.dart';
import 'package:estimator_app/src/presentation/bloc/booking/find_booking/find_booking_bloc.dart';
import 'package:estimator_app/src/presentation/bloc/booking/set_estimateed_amount/set_estimated_amount_bloc.dart';
import 'package:estimator_app/src/presentation/views/authenticated/dashboard/content/booking/map_booking/map_booking.dart';
import 'package:estimator_app/translations/locale_keys.g.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:video_player/video_player.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';

class BookingPreview extends StatefulWidget {
  final String? payload;
  const BookingPreview({Key? key, this.payload}) : super(key: key);

  @override
  _BookingPreviewState createState() => _BookingPreviewState();
}

class _BookingPreviewState extends State<BookingPreview> {
  showEnlargedPicture(String asset) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              insetPadding: EdgeInsets.zero,
              contentPadding: EdgeInsets.zero,
              content: CachedNetworkImage(imageUrl: asset),
              actions: [
                TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: Text(LocaleKeys.close.tr()))
              ],
            ));
  }

  showEnlargedVideo(String asset) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              insetPadding: EdgeInsets.zero,
              contentPadding: EdgeInsets.zero,
              content:
                  SizedBox(width: 100.0.w, child: VideoCreator(url: asset)),
            ));
  }

  showClientDetails(EstimatorBookingModel driverBookingModel) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text(LocaleKeys.client_details.tr()),
              content: clientDetails(driverBookingModel),
            ));
  }

  Future<void> initDataWithPayload() async {
    final findBookingBloc = BlocProvider.of<FindBookingBloc>(context);
    findBookingBloc.add(FindBookEvent(widget.payload!));
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if (widget.payload != null) {
      initDataWithPayload();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Text(LocaleKeys.booking_information.tr(),
            style: const TextStyle(color: Colors.black)),
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: BlocBuilder<FindBookingBloc, FindBookingState>(
          builder: (context, state) {
        if (state is LoadingFindBookingState) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        } else if (state is SuccessFindBookingState) {
          EstimatorBookingModel estimatorBookingModel = state.estimatorBookingModel!;

          String estimatedAmount = estimatorBookingModel.estimatedAmount != null
              ? estimatorBookingModel.estimatedAmount.toString()
              : 'TBA';

          bool hasAdditionalKchra =
          estimatorBookingModel.hasAdditionalKachra == 1 ? true : false;
          String additionalAmount = estimatorBookingModel.additionalAmount != null
              ? estimatorBookingModel.additionalAmount.toString()
              : 'TBA';

          String address = estimatorBookingModel.address!.country!;
          String clientName = estimatorBookingModel.user!.name!;

          bool isCOD = estimatorBookingModel.paymentMethod == 'Cash-On-Delivery' &&
              estimatorBookingModel.quoteStatus == 'Accepted'
              ? true
              : false;
          bool isDriverBookingPending =
          estimatorBookingModel.driverBookingStatus == 'Pending'
                  ? true
                  : false;

          bool isButtonDisabled =
          estimatorBookingModel.estimatedAmount != null ? false : true;

          var outputFormat = DateFormat('MMMM dd, yyyy | hh:mm a');
          DateTime resDateTime = DateTime.parse(estimatorBookingModel.updatedAt!);

          String dateTime = outputFormat.format(resDateTime).toString();

          bool isVideo = estimatorBookingModel.assetType == "Video" ? true : false;

          bool isForRemoval = estimatorBookingModel.bookingPurpose == 'Removal' ? true : false;

          return Center(
            child: Stack(
              children: [
                isVideo
                    ? GestureDetector(
                        onTap: () =>
                            showEnlargedVideo(estimatorBookingModel.asset!),
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: Container(
                            height: 450,
                            width: 100.0.w,
                            decoration: BoxDecoration(
                              color: Colors.black,
                              border:
                                  Border.all(color: Colors.black, width: 2.0),
                              image: DecorationImage(
                                  image: const AssetImage(
                                      'assets/img/booking1.png'),
                                  fit: BoxFit.cover,
                                  colorFilter: ColorFilter.mode(
                                      Colors.grey.withOpacity(0.5),
                                      BlendMode.dstATop)),
                            ),
                            child: const Center(
                              child: Icon(Icons.video_collection_outlined,
                                  color: Colors.white, size: 50.0),
                            ),
                          ),
                        ),
                      )
                    : GestureDetector(
                        onTap: () =>
                            showEnlargedPicture(estimatorBookingModel.asset!),
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: Container(
                            height: 450,
                            width: 100.0.w,
                            decoration: BoxDecoration(
                              color: Colors.black,
                              border:
                                  Border.all(color: Colors.black, width: 2.0),
                              image: DecorationImage(
                                  image: CachedNetworkImageProvider(
                                      estimatorBookingModel.asset!),
                                  fit: BoxFit.cover,
                                  colorFilter: ColorFilter.mode(
                                      Colors.grey.withOpacity(0.5),
                                      BlendMode.dstATop)),
                            ),
                            child: const Center(
                              child: Icon(Icons.image,
                                  color: Colors.white, size: 50.0),
                            ),
                          ),
                        ),
                      ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    height: 380,
                    width: 100.0.w,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(20.0),
                          topRight: Radius.circular(20.0)),
                      border: Border.all(color: Colors.black, width: 2.0),
                    ),
                    child: Column(
                      children: [
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0),
                          child: SizedBox(
                            height: 250,
                            child: SingleChildScrollView(
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      GestureDetector(
                                        onTap: () => CopyClipboard()
                                            .copyToClipboard(context,
                                            estimatorBookingModel.id!),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              LocaleKeys.booking_id.tr(),
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 15.0.sp),
                                            ),
                                            const SizedBox(height: 5.0),
                                            Text(
                                              TextChecker().checkText(
                                                  estimatorBookingModel.id!),
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 10.0.sp),
                                            )
                                          ],
                                        ),
                                      ),
                                      const Spacer(),
                                      Column(
                                        children: [
                                          Text(
                                            '$estimatedAmount ر.ق ',
                                            style: TextStyle(
                                                color: Colors.green,
                                                fontWeight: FontWeight.w900,
                                                fontSize: 16.0.sp,
                                                decoration: hasAdditionalKchra
                                                    ? TextDecoration.lineThrough
                                                    : null,
                                                decorationThickness: 3.0,
                                                decorationColor: Colors.black),
                                          ),
                                          hasAdditionalKchra
                                              ? Text(
                                                  '$additionalAmount ر.ق ',
                                                  style: TextStyle(
                                                      color: Colors.green,
                                                      fontWeight:
                                                          FontWeight.w900,
                                                      fontSize: 12.0.sp),
                                                )
                                              : const SizedBox(height: 0),
                                        ],
                                      )
                                    ],
                                  ),
                                  const SizedBox(height: 20.0),
                                  Row(
                                    children: [
                                      Icon(Icons.book, color: isForRemoval ? Colors.green : Colors.red),
                                      const SizedBox(width: 5.0),
                                      Text(estimatorBookingModel.bookingPurpose!, style: TextStyle(color: isForRemoval ? Colors.green : Colors.red, fontWeight: FontWeight.w700),)
                                    ],
                                  ),
                                  const SizedBox(height: 20.0),
                                  Row(
                                    children: [
                                      const Icon(Icons.calendar_today_outlined),
                                      const SizedBox(width: 5.0),
                                      Text(dateTime)
                                    ],
                                  ),
                                  const SizedBox(height: 15.0),
                                  Row(
                                    children: [
                                      const Icon(Icons.location_on_outlined),
                                      const SizedBox(width: 5.0),
                                      GestureDetector(
                                          onTap: () async {
                                            bool serviceEnabled;
                                            LocationPermission permission;

                                            serviceEnabled = await Geolocator
                                                .isLocationServiceEnabled();
                                            if (!serviceEnabled) {
                                              return Future.error(
                                                  'Location services are disabled.');
                                            }

                                            permission = await Geolocator
                                                .checkPermission();
                                            if (permission ==
                                                LocationPermission.denied) {
                                              permission = await Geolocator
                                                  .requestPermission();
                                              if (permission ==
                                                  LocationPermission.denied) {
                                                return Future.error(
                                                    'Location permissions are denied');
                                              }
                                            }

                                            if (permission ==
                                                LocationPermission
                                                    .deniedForever) {
                                              // Permissions are denied forever, handle appropriately.
                                              return Future.error(
                                                  'Location permissions are permanently denied, we cannot request permissions.');
                                            }

                                            Position location = await Geolocator
                                                .getCurrentPosition();
                                            List<Placemark> p =
                                                await placemarkFromCoordinates(
                                                    location.latitude,
                                                    location.longitude);
                                            Placemark place = p[0];

                                            String address =
                                                place.street.toString() +
                                                    ', ' +
                                                    place.administrativeArea
                                                        .toString();

                                            Navigator.of(context).pop();
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        MapBooking(
                                                          originLat:
                                                              location.latitude,
                                                          originLng: location
                                                              .longitude,
                                                          originAddress:
                                                              address,
                                                          destinationLat: double.parse(
                                                              estimatorBookingModel
                                                                  .address!
                                                                  .latitude
                                                                  .toString()),
                                                          destinationLng: double.parse(
                                                              estimatorBookingModel
                                                                  .address!
                                                                  .longitude
                                                                  .toString()),
                                                          destinationAddress:
                                                          estimatorBookingModel
                                                                  .address!
                                                                  .name,
                                                        )));
                                          },
                                          child: Text(LocaleKeys.open_maps.tr(),
                                              style: const TextStyle(
                                                  color: Colors.blue)))
                                    ],
                                  ),
                                  const SizedBox(height: 15.0),
                                  Row(
                                    children: [
                                      const Icon(Icons.person_outline_outlined),
                                      const SizedBox(width: 5.0),
                                      Text(TextChecker().checkText(clientName)),
                                      const Spacer(),
                                      IconButton(
                                          onPressed: () => launch(
                                              "tel://${estimatorBookingModel.user!.phoneNumber}"),
                                          icon:
                                              const Icon(Icons.phone_in_talk)),
                                      IconButton(
                                          onPressed: () {
                                            showClientDetails(
                                                estimatorBookingModel);
                                          },
                                          icon: const Icon(Icons.menu))
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        const Spacer(),
                        Container(
                          height: 90,
                          width: 100.0.w,
                          decoration: BoxDecoration(
                            color: Colors.orange[50],
                          ),
                          child: Padding(
                            padding:
                                const EdgeInsets.fromLTRB(25.0, 10.0, 25.0, 0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Text('Estimate:'),
                                    Text(
                                      '$estimatedAmount ر.ق ',
                                      style: const TextStyle(
                                          fontWeight: FontWeight.w900),
                                    )
                                  ],
                                ),
                                const Spacer(),
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(10.0),
                                  child: ElevatedButton(
                                      style: ButtonStyle(
                                          backgroundColor:
                                          MaterialStateProperty
                                              .all(AppTheme
                                              .light
                                              .accentColor)),
                                      onPressed: () {
                                        TextEditingController estimatedAmountController = TextEditingController();

                                        showDialog(
                                          context: context,
                                          builder: (context) => AlertDialog(
                                            title: const Text('Set an Estimated Amount for this booking? $confirmEmoji'),
                                            content: TextField(
                                              controller: estimatedAmountController,
                                              keyboardType: TextInputType.number,
                                              decoration: InputDecoration(
                                                  labelText: 'Amount in QAR',
                                                  labelStyle: TextStyle(
                                                      color: Colors.grey,
                                                      fontSize: 10.0.sp),
                                              ),
                                            ),
                                            actions: [
                                              BlocBuilder<SetEstimatedAmountBloc, SetEstimatedAmountState>(
                                                builder: (context, state) {
                                                  if(state is LoadingSetEstimatedAmountState){
                                                    return const CircularProgressIndicator();
                                                  } else {
                                                    return TextButton(
                                                        onPressed: (){
                                                          SetEstimatedAmountParams setEstimatedAmountParams = SetEstimatedAmountParams(
                                                              bookingId: estimatorBookingModel.id,
                                                              estimatedAmount: int.parse(estimatedAmountController.text)
                                                          );

                                                          final setEstimatedAmountBloc = BlocProvider.of<SetEstimatedAmountBloc>(context);
                                                          setEstimatedAmountBloc.add(SetAmountEvent(setEstimatedAmountParams));
                                                        },
                                                        child: Text('${LocaleKeys.confirm.tr()} $yesEmoji')
                                                    );
                                                  }
                                                }
                                              ),
                                              BlocBuilder<SetEstimatedAmountBloc, SetEstimatedAmountState>(
                                                builder: (context, state) {
                                                  if(state is LoadingSetEstimatedAmountState){
                                                    return const SizedBox(height: 0);
                                                  }else {
                                                    return TextButton(
                                                        onPressed: (){

                                                        },
                                                        child: Text('${LocaleKeys.cancel.tr()} $noEmoji')
                                                    );
                                                  }
                                                }
                                              )
                                            ],
                                          )
                                        );
                                      },
                                      child: const Padding(
                                        padding:
                                        EdgeInsets.all(
                                            12.0),
                                        child: Text('Set Amount', style: TextStyle(color: Colors.white),
                                        ),
                                      )),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
        } else {
          return const Center(
            child: Text('Failed to load booking.'),
          );
        }
      }),
    );
  }

  Widget clientDetails(EstimatorBookingModel driverBookingModel) {
    return SizedBox(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              children: [
                Text(LocaleKeys.full_name.tr()),
                Flexible(child: Text(driverBookingModel.user!.name!))
              ],
            ),
            driverBookingModel.user!.email != null ? Row(
              children: [
                Text(LocaleKeys.email.tr()),
                Flexible(child: Text(driverBookingModel.user!.email!))
              ],
            ) : const SizedBox(height: 0),
            Row(
              children: [
                Text(LocaleKeys.phone_number.tr()),
                Text(driverBookingModel.user!.phoneNumber!)
              ],
            ),
            driverBookingModel.user!.country != null ? Row(
              children: [
                Text(LocaleKeys.country.tr()),
                Text(driverBookingModel.user!.country!)
              ],
            ) : const SizedBox(height: 0),
            const SizedBox(height: 30.0),
            Text(LocaleKeys.address.tr()),
            const SizedBox(height: 10.0),
            Row(
              children: [
                Text(LocaleKeys.full_name.tr()),
                Flexible(
                  child: Text(driverBookingModel.user!.address!.name!),
                )
              ],
            ),
            Row(
              children: [
                Text(LocaleKeys.street.tr()),
                Flexible(
                  child: Text(driverBookingModel.user!.address!.street!),
                )
              ],
            ),
            Row(
              children: [
                Text(LocaleKeys.country.tr()),
                Flexible(
                  child: Text(driverBookingModel.user!.address!.country!),
                )
              ],
            ),
            Row(
              children: [
                Text(LocaleKeys.locality.tr()),
                Flexible(
                  child: Text(driverBookingModel.user!.address!.locality!),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

class VideoCreator extends StatefulWidget {
  final String? url;

  const VideoCreator({Key? key, this.url}) : super(key: key);

  @override
  _VideoCreatorState createState() => _VideoCreatorState();
}

class _VideoCreatorState extends State<VideoCreator> {
  VideoPlayerController? _controller;
  ChewieController? chewieController;

  @override
  void initState() {
    super.initState();

    _controller = VideoPlayerController.network(widget.url!);
    chewieController = ChewieController(
        videoPlayerController: _controller!,
        looping: true,
        showControls: true,
        allowFullScreen: false,
        deviceOrientationsOnEnterFullScreen: [
          DeviceOrientation.landscapeLeft,
          DeviceOrientation.landscapeRight
        ]);
  }

  @override
  Widget build(BuildContext context) {
    _controller = VideoPlayerController.network(widget.url!);

    chewieController = ChewieController(
        videoPlayerController: _controller!,
        looping: true,
        showControls: true,
        allowFullScreen: false,
        deviceOrientationsOnEnterFullScreen: [
          DeviceOrientation.landscapeLeft,
          DeviceOrientation.landscapeRight
        ]);

    return AspectRatio(
        aspectRatio: _controller!.value.aspectRatio,
        child: Chewie(
          controller: chewieController!,
        ));
  }

  @override
  void dispose() {
    _controller!.dispose();
    chewieController!.dispose();
    super.dispose();
  }
}
