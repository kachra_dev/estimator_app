import 'package:estimator_app/src/presentation/views/authenticated/dashboard/content/booking/map_booking/show_directions.dart';
import 'package:estimator_app/src/presentation/views/authenticated/dashboard/content/booking/map_booking/show_marker.dart';
import 'package:estimator_app/translations/locale_keys.g.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

class MapBooking extends StatefulWidget {
  final double? destinationLat;
  final double? destinationLng;
  final String? destinationAddress;
  final double? originLat;
  final double? originLng;
  final String? originAddress;
  const MapBooking(
      {Key? key,
      this.destinationLat,
      this.destinationLng,
      this.originLat,
      this.originLng,
      this.destinationAddress,
      this.originAddress})
      : super(key: key);

  @override
  _MapBookingState createState() => _MapBookingState();
}

class _MapBookingState extends State<MapBooking> {
  int selectedTabIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          LocaleKeys.maps_and_directions.tr(),
          style: const TextStyle(color: Colors.black),
        ),
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: ShowDirections(
        destinationLat: widget.destinationLat,
        destinationLng: widget.destinationLng,
        destinationAddress: widget.destinationAddress,
        originLat: widget.originLat,
        originLng: widget.originLng,
        originAddress: widget.originAddress,
      ),
    );
  }
}
