
import 'package:estimator_app/src/presentation/bloc/booking/booking_bloc.dart';
import 'package:estimator_app/src/presentation/views/authenticated/dashboard/content/booking/body_booking.dart';
import 'package:estimator_app/src/presentation/views/authenticated/dashboard/content/booking/drawer/booking_drawer.dart';
import 'package:estimator_app/src/presentation/views/authenticated/dashboard/content/booking/site_visit_schedules/table_calendar_sched.dart';
import 'package:estimator_app/translations/locale_keys.g.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Booking extends StatefulWidget {
  const Booking({Key? key}) : super(key: key);

  @override
  State<Booking> createState() => _BookingState();
}

class _BookingState extends State<Booking> {
  @override
  void initState() {
    super.initState();
    getBookings();
  }

  Future<void> getBookings() async {
    ///get all assigned bookings
    final bookingBloc = BlocProvider.of<BookingBloc>(context);
    bookingBloc.add(const GetAllBookingsEvent());
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            title: Image.asset('assets/img/logo.png', scale: 4.0),
            centerTitle: true,
            iconTheme: const IconThemeData(color: Colors.black),
            backgroundColor: const Color(0xFFFEFBF3),
            bottom: const TabBar(
              tabs: [
                Tab(icon: Icon(Icons.calendar_today)),
                Tab(icon: Icon(Icons.my_library_books_outlined)),
              ],
            ),
          ),
          backgroundColor: const Color(0xFFFEFBF3),
          body: const TabBarView(
            children: [
              TableCalendarSched(),
              BodyBooking()
            ],
          ),
          drawer: const BookingDrawer(),
        ),
      ),
    );
  }
}
