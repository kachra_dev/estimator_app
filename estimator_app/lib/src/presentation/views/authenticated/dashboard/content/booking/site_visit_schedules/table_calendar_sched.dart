import 'dart:collection';

import 'package:estimator_app/src/core/utils/table_calendar_utils.dart';
import 'package:estimator_app/src/data/models/site_visit_schedules/detailed_site_visit_schedule_model.dart';
import 'package:estimator_app/src/data/models/site_visit_schedules/site_visit_schedules_model.dart';
import 'package:estimator_app/src/presentation/bloc/site_visit_schedules/find_site_visit_schedules/find_site_visit_schedules_bloc.dart';
import 'package:estimator_app/src/presentation/bloc/site_visit_schedules/site_visit_schedules_bloc.dart';
import 'package:estimator_app/src/presentation/views/authenticated/dashboard/content/booking/site_visit_schedules/detailed_site_visit_schedule.dart';
import 'package:estimator_app/translations/locale_keys.g.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:easy_localization/easy_localization.dart';

class TableCalendarSched extends StatefulWidget {
  const TableCalendarSched({Key? key}) : super(key: key);

  @override
  _TableCalendarSchedState createState() => _TableCalendarSchedState();
}

class _TableCalendarSchedState extends State<TableCalendarSched> {
  late final ValueNotifier<List<Event>> _selectedEvents;
  CalendarFormat _calendarFormat = CalendarFormat.month;
  RangeSelectionMode _rangeSelectionMode = RangeSelectionMode.toggledOff; // Can be toggled on/off by longpressing a date
  DateTime _focusedDay = DateTime.now();
  DateTime? _selectedDay;
  DateTime? _rangeStart;
  DateTime? _rangeEnd;

  @override
  void initState() {
    super.initState();

    _selectedDay = _focusedDay;
    _selectedEvents = ValueNotifier(_getEventsForDay(_selectedDay!));
  }

  @override
  void dispose() {
    _selectedEvents.dispose();
    super.dispose();
  }

  List<Event> _getEventsForDay(DateTime day) {
    var logger = Logger();

    final siteVisitSchedulesBloc = BlocProvider.of<SiteVisitSchedulesBloc>(context);

    if(siteVisitSchedulesBloc.state is SuccessSiteVisitSchedulesState){
      final List<SiteVisitSchedulesModel> siteVisitSchedulesModel = siteVisitSchedulesBloc.state.siteVisitSchedulesList!;

      final _kEventSource = {
        for (var item in siteVisitSchedulesModel)
          DateTime.utc(item.scheduledDate!.year, item.scheduledDate!.month, item.scheduledDate!.day) :
            List<Event>.from(siteVisitSchedulesModel.where((element) => DateTime.utc(item.scheduledDate!.year, item.scheduledDate!.month, item.scheduledDate!.day).difference(DateTime.utc(element.scheduledDate!.year, element.scheduledDate!.month, element.scheduledDate!.day)).inDays == 0).toList().map((e) => Event(e)))

      };

      final kEventsList = LinkedHashMap<DateTime, List<Event>>(
        equals: isSameDay,
        hashCode: getHashCode,
      )..addAll(_kEventSource);

      return kEventsList[day] ?? [];
    } else {
      return [];
    }
  }

  void _onDaySelected(DateTime selectedDay, DateTime focusedDay) {
    if (!isSameDay(_selectedDay, selectedDay)) {
      setState(() {
        _selectedDay = selectedDay;
        _focusedDay = focusedDay;
        _rangeStart = null; // Important to clean those
        _rangeEnd = null;
        _rangeSelectionMode = RangeSelectionMode.toggledOff;
      });

      _selectedEvents.value = _getEventsForDay(selectedDay);
    }
  }

  Future<void> getSiteVisitSchedules() async {
    ///get all assigned bookings
    final siteVisitSchedulesBloc = BlocProvider.of<SiteVisitSchedulesBloc>(context);
    siteVisitSchedulesBloc.add(const GetSiteVisitSchedulesEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(LocaleKeys.schedule.tr(), style: const TextStyle(color: Colors.black),),
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: BlocBuilder<SiteVisitSchedulesBloc, SiteVisitSchedulesState>(
        builder: (context, state) {
          if(state is LoadingSiteVisitSchedulesState){
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is SuccessSiteVisitSchedulesState) {
            return Column(
              children: [
                TableCalendar<Event>(
                  firstDay: kFirstDay,
                  lastDay: kLastDay,
                  focusedDay: _focusedDay,
                  selectedDayPredicate: (day) => isSameDay(_selectedDay, day),
                  rangeStartDay: _rangeStart,
                  rangeEndDay: _rangeEnd,
                  calendarFormat: _calendarFormat,
                  rangeSelectionMode: _rangeSelectionMode,
                  eventLoader: _getEventsForDay,
                  startingDayOfWeek: StartingDayOfWeek.monday,
                  calendarStyle: const CalendarStyle(
                    // Use `CalendarStyle` to customize the UI
                    outsideDaysVisible: false,
                  ),
                  onDaySelected: _onDaySelected,
                  onFormatChanged: (format) {
                    if (_calendarFormat != format) {
                      setState(() {
                        _calendarFormat = format;
                      });
                    }
                  },
                  onPageChanged: (focusedDay) {
                    _focusedDay = focusedDay;
                  },
                ),
                const SizedBox(height: 8.0),
                Expanded(
                  child: ValueListenableBuilder<List<Event>>(
                    valueListenable: _selectedEvents,
                    builder: (context, value, _) {
                      return value.isNotEmpty ? ListView.builder(
                        itemCount: value.length,
                        itemBuilder: (context, index) {
                          Event event = value[index];

                          var outputFormat = DateFormat('MMMM dd, yyyy | hh:mm a');
                          DateTime resDateTime = DateTime.parse(event.siteVisitSchedulesModel!.scheduledDate.toString());
                          String dateTime = outputFormat.format(resDateTime).toString();

                          return Container(
                            margin: const EdgeInsets.symmetric(
                              horizontal: 12.0,
                              vertical: 4.0,
                            ),
                            decoration: BoxDecoration(
                              border: Border.all(),
                              borderRadius: BorderRadius.circular(12.0),
                            ),
                            child: ListTile(
                              onTap: (){
                                var logger = Logger();

                                logger.i(event.siteVisitSchedulesModel!.id);

                                final findSiteVisitBloc = BlocProvider.of<FindSiteVisitSchedulesBloc>(context);
                                findSiteVisitBloc.add(FindSiteScheduleEvent(event.siteVisitSchedulesModel!.id));

                                Navigator.push(context, MaterialPageRoute(builder: (context) => const DetailedSiteVisitSchedule()));
                              },
                              title: Text(dateTime),
                            ),
                          );
                        },
                      ) : Center(
                        child: Text('${LocaleKeys.no_selected_booking.tr()} ${LocaleKeys.please_select_a_date.tr()}'),
                      );
                    },
                  ),
                ),
              ],
            );
          } else {
            return const Center(
              child: Text(
                'Failed to load site visit schedules'
              ),
            );
          }
        }
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () => getSiteVisitSchedules(),
        icon: const Icon(CupertinoIcons.arrow_2_squarepath),
        label: Text(LocaleKeys.sync_schedules.tr()),
      ),
    );
  }
}
