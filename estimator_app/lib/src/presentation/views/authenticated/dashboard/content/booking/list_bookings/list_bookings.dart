import 'package:cached_network_image/cached_network_image.dart';
import 'package:chewie/chewie.dart';
import 'package:estimator_app/src/core/utils/hive/get_hive.dart';
import 'package:estimator_app/src/data/models/booking/estimator_booking_model.dart';
import 'package:estimator_app/src/data/models/hive/driver_hive.dart';
import 'package:estimator_app/src/data/models/hive/profile_hive.dart';
import 'package:estimator_app/src/presentation/bloc/booking/booking_bloc.dart';
import 'package:estimator_app/src/presentation/bloc/booking/find_booking/find_booking_bloc.dart';
import 'package:estimator_app/src/presentation/views/authenticated/dashboard/content/booking/preview/booking_preview.dart';
import 'package:estimator_app/translations/locale_keys.g.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:sizer/sizer.dart';
import 'package:video_player/video_player.dart';
import 'package:easy_localization/easy_localization.dart';

class ListBookings extends StatefulWidget {
  const ListBookings({Key? key}) : super(key: key);

  @override
  _ListBookingsState createState() => _ListBookingsState();
}

class _ListBookingsState extends State<ListBookings> {
  Future<void> getBookings() async {
    // DriverHive driver = await GetHive().getCurrentDriverProfile();
    //
    // final bookingBloc = BlocProvider.of<BookingBloc>(context);
    // bookingBloc.add(GetAllBookingsEvent(driver.id!));
  }

  List<Color> colors = [
    const Color(0xFFFFBD35),
    const Color(0xFF3FA796),
    const Color(0xFFA6CF98),
    const Color(0xFFF05454)
  ];

  List<String> image = [
    'assets/img/booking1.png',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('List of Assigned Bookings',
            style: TextStyle(color: Colors.black)),
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: BlocBuilder<BookingBloc, BookingState>(builder: (context, state) {
        if (state is SuccessBookingState) {
          List<EstimatorBookingModel> resBookingList = state.booking!;

          return RefreshIndicator(
            onRefresh: () => getBookings(),
            child: resBookingList.isNotEmpty
                ? ListView.builder(
                    itemCount: resBookingList.length,
                    itemBuilder: (context, index) {
                      EstimatorBookingModel booking = resBookingList[index];

                      var outputFormat = DateFormat('MMMM dd, yyyy | hh:mm a');
                      DateTime resDateTime = DateTime.parse(booking.updatedAt!);

                      String dateTime =
                          outputFormat.format(resDateTime).toString();

                      String amount = booking.estimatedAmount != null
                          ? booking.estimatedAmount!.toString()
                          : 'TBA';

                      return GestureDetector(
                        onTap: () {
                          final findBookingBloc =
                              BlocProvider.of<FindBookingBloc>(context);
                          findBookingBloc.add(FindBookEvent(booking.id));

                          Navigator.push(context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      const BookingPreview()));
                          // if (booking.assetType == 'Image') {
                          //   Navigator.push(context, MaterialPageRoute(builder: (context) => BookingPreview(booking: booking, isVideo: false)));
                          // } else {
                          //   Navigator.push(context, MaterialPageRoute(builder: (context) => BookingPreview(booking: booking, isVideo: true)));
                          // }
                        },
                        child: Column(
                          children: [
                            Container(
                              height: 240,
                              width: 90.0.w,
                              decoration: BoxDecoration(
                                color: colors[2],
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: Card(
                                child: Column(
                                  children: [
                                    Container(
                                      height: 100,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  image[index % image.length]),
                                              fit: BoxFit.cover)),
                                    ),
                                    ListTile(
                                      title: Text(
                                        dateTime,
                                        style: TextStyle(
                                            color: Colors.yellow[900]),
                                      ),
                                      trailing: IconButton(
                                          onPressed: () async {},
                                          icon:
                                              const Icon(Icons.chevron_right)),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(height: 10)
                          ],
                        ),
                      );
                    })
                : RefreshIndicator(
                    onRefresh: () => getBookings(),
                    child: Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset('assets/img/pending.png', scale: 2.0),
                          Text(LocaleKeys.no_bookings.tr()),
                        ],
                      ),
                    ),
                  ),
          );
        } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      }),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () => getBookings(),
        icon: const Icon(CupertinoIcons.arrow_2_circlepath),
        label: Text(LocaleKeys.sync_bookings.tr()),
      ),
    );
  }
}
