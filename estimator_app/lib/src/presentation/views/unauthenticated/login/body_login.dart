import 'package:estimator_app/main.dart';
import 'package:estimator_app/src/config/themes/app_theme.dart';
import 'package:estimator_app/src/core/params/auth_params.dart';
import 'package:estimator_app/src/core/utils/helper/text_checker.dart';
import 'package:estimator_app/src/core/utils/services/firebase_service.dart';
import 'package:estimator_app/src/presentation/bloc/auth/auth_bloc.dart';
import 'package:estimator_app/translations/locale_keys.g.dart';
import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logger/logger.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:sizer/sizer.dart';

class BodyLogin extends StatefulWidget {
  const BodyLogin({Key? key}) : super(key: key);

  @override
  _BodyLoginState createState() => _BodyLoginState();
}

class _BodyLoginState extends State<BodyLogin> {
  bool hidePassword = true;

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 33.0.h,
            decoration: BoxDecoration(
                color: AppTheme.light.accentColor,
                image: const DecorationImage(
                    image: AssetImage('assets/img/login.png'),
                    fit: BoxFit.cover),
                borderRadius: const BorderRadius.only(
                    bottomLeft: Radius.circular(100.0),
                    bottomRight: Radius.circular(100.0))),
          ),
          SizedBox(
            height: 67.0.h,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                      padding: const EdgeInsets.only(top: 30.0),
                      child: Image.asset(
                        'assets/img/logo.png',
                        scale: 3.0,
                      )),
                  const SizedBox(height: 20.0),
                  // ElevatedButton(
                  //     style: ButtonStyle(
                  //         backgroundColor:
                  //             MaterialStateProperty.all(Colors.white)),
                  //     onPressed: () async {
                  //       var logger = Logger();
                  //
                  //       await FirebaseService().signOutFromGoogle();
                  //       String? email =
                  //           await FirebaseService().signInWithGoogle();
                  //
                  //       final authBloc = BlocProvider.of<AuthBloc>(context);
                  //       authBloc.add(LoginAuthGoogleEvent(email, context));
                  //
                  //       logger.i(email);
                  //     },
                  //     child: SizedBox(
                  //       width: 65.0.w,
                  //       child: Row(
                  //         mainAxisAlignment: MainAxisAlignment.center,
                  //         children: [
                  //           Image.asset('assets/img/google.png', scale: 18.0),
                  //           const SizedBox(width: 10.0),
                  //           Text(
                  //             LocaleKeys.sign_in_with_google.tr(),
                  //             style: const TextStyle(color: Colors.black),
                  //           )
                  //         ],
                  //       ),
                  //     )),
                  // const Padding(
                  //   padding: EdgeInsets.all(10.0),
                  //   child: Text('Or'),
                  // ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20.0, 0, 20.0, 10.0),
                    child: TextFormField(
                      controller: emailController,
                      decoration: InputDecoration(
                        labelText: LocaleKeys.email.tr(),
                        labelStyle: TextStyle(
                            color: Colors.black, fontSize: 10.0.sp),
                        filled: true,
                        fillColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: const BorderSide(
                            color: Colors.grey,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: const BorderSide(
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20.0, 0, 20.0, 10.0),
                    child: TextFormField(
                      controller: passwordController,
                      decoration: InputDecoration(
                          labelText: LocaleKeys.password.tr(),
                          labelStyle: TextStyle(
                              color: Colors.black, fontSize: 10.0.sp),
                          filled: true,
                          fillColor: Colors.white,
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: const BorderSide(
                              color: Colors.grey,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: const BorderSide(
                              color: Colors.grey,
                            ),
                          ),
                          suffixIcon: IconButton(
                              onPressed: hidePass,
                              icon: const Icon(Icons.remove_red_eye))),
                      obscureText: hidePassword,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(50.0, 30.0, 50.0, 0),
                    child: SizedBox(
                      width: 100.0.w,
                      child: BlocBuilder<AuthBloc, AuthState>(
                          builder: (context, state) {
                        if (state is AuthLoadingState) {
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        } else {
                          return ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                      AppTheme.light.accentColor)),
                              onPressed: () {
                                var logger = Logger();

                                logger.i(emailController.text);
                                logger.i(passwordController.text);

                                AuthParams authParams = AuthParams(
                                    email: emailController.text,
                                    password: passwordController.text);

                                final authBloc =
                                    BlocProvider.of<AuthBloc>(context);
                                authBloc.add(LoginAuthEvent(authParams, context));
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: Text(LocaleKeys.log_in.tr()),
                              ));
                        }
                      }),
                    ),
                  ),
                  // Directionality(
                  //   textDirection: ltrTextDirection,
                  //   child: Row(
                  //     mainAxisAlignment: MainAxisAlignment.center,
                  //     crossAxisAlignment: CrossAxisAlignment.center,
                  //     children: [
                  //       const Text('Don\'t have an account? ',
                  //           style: TextStyle(fontSize: 12.0)),
                  //       TextButton(
                  //         onPressed: () async {
                  //           showCupertinoModalBottomSheet(
                  //               context: context,
                  //               backgroundColor: Colors.transparent,
                  //               builder: (context) =>
                  //                   const RegistrationModalSheet());
                  //         },
                  //         child: Text(LocaleKeys.register_here.tr(),
                  //             style: const TextStyle(fontSize: 12.0)),
                  //         style: TextButton.styleFrom(padding: EdgeInsets.zero),
                  //       ),
                  //     ],
                  //   ),
                  // ),
                  Directionality(
                    textDirection: ltrTextDirection,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Text('Choose Language:'),
                        TextButton(
                          onPressed: () async {
                            await MyApp.navigatorKey.currentContext!
                                .setLocale(const Locale('en'));
                            Navigator.popUntil(
                                context, (Route<dynamic> route) => route.isFirst);

                            MyApp.restartApp(context);
                          },
                          child: const Text('English',
                              style: TextStyle(fontSize: 12.0)),
                          style: TextButton.styleFrom(padding: EdgeInsets.zero),
                        ),
                        TextButton(
                          onPressed: () async {
                            await MyApp.navigatorKey.currentContext!
                                .setLocale(const Locale('ar'));
                            Navigator.popUntil(
                                context, (Route<dynamic> route) => route.isFirst);

                            MyApp.restartApp(context);
                          },
                          child: const Text('عربي',
                              style: TextStyle(fontSize: 12.0)),
                          style: TextButton.styleFrom(padding: EdgeInsets.zero),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  hidePass() {
    setState(() {
      if (hidePassword) {
        hidePassword = false;
      } else {
        hidePassword = true;
      }
    });
  }
}
