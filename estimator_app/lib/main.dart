import 'dart:ffi';

import 'package:estimator_app/src/config/routes/app_routes.dart';
import 'package:estimator_app/src/config/themes/app_theme.dart';
import 'package:estimator_app/src/core/utils/constants.dart';
import 'package:estimator_app/src/core/utils/helper/notification_api.dart';
import 'package:estimator_app/src/data/models/hive/address_hive.dart';
import 'package:estimator_app/src/data/models/hive/driver_hive.dart';
import 'package:estimator_app/src/data/models/hive/profile_hive.dart';
import 'package:estimator_app/src/injector.dart';
import 'package:estimator_app/src/presentation/bloc/auth/auth_bloc.dart';
import 'package:estimator_app/src/presentation/bloc/booking/booking_bloc.dart';
import 'package:estimator_app/src/presentation/bloc/booking/find_booking/find_booking_bloc.dart';
import 'package:estimator_app/src/presentation/bloc/booking/set_estimateed_amount/set_estimated_amount_bloc.dart';
import 'package:estimator_app/src/presentation/bloc/site_visit_schedules/find_site_visit_schedules/find_site_visit_schedules_bloc.dart';
import 'package:estimator_app/src/presentation/bloc/site_visit_schedules/site_visit_schedules_bloc.dart';
import 'package:estimator_app/src/presentation/views/authenticated/dashboard/content/booking/preview/booking_preview.dart';
import 'package:estimator_app/translations/codegen_loader.g.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:hive/hive.dart';
import 'package:logger/logger.dart';
import 'package:rxdart/rxdart.dart';
import 'package:sizer/sizer.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
// import 'package:awesome_notifications/awesome_notifications.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final appDocumentDir = await path_provider.getApplicationDocumentsDirectory();
  Hive.init(appDocumentDir.path);
  await initializeDependencies();

  ///Register Hive TypeAdapters
  Hive.registerAdapter(ProfileHiveAdapter());
  Hive.registerAdapter(AddressHiveAdapter());
  Hive.registerAdapter(DriverHiveAdapter());

  await Firebase.initializeApp();

  await EasyLocalization.ensureInitialized();

  runApp(EasyLocalization(
      supportedLocales: const [Locale('en'), Locale('ar')],
      path:
          'assets/translations', // <-- change the path of the translation files
      fallbackLocale: const Locale('en'),
      assetLoader: const CodegenLoader(),
      child: const MyApp()));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  static final GlobalKey<NavigatorState> navigatorKey =
      GlobalKey<NavigatorState>();

  static void restartApp(BuildContext context) {
    context.findAncestorStateOfType<_MyAppState>()!.restartApp();
  }

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Key key = UniqueKey();

  void restartApp() {
    setState(() {
      key = UniqueKey();
    });
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<AuthBloc>(
              create: (_) => injector()..add(const GetStoredAuthEvent())),
          BlocProvider<BookingBloc>(
              create: (_) => injector()..add(const InitialBookingEvent())),
          BlocProvider<FindBookingBloc>(
              create: (_) => injector()..add(const FindBookEvent(null))),
          BlocProvider<SiteVisitSchedulesBloc>(
              create: (_) => injector()..add(const GetSiteVisitSchedulesEvent())),
          BlocProvider<FindSiteVisitSchedulesBloc>(
              create: (_) => injector()..add(const FindSiteScheduleEvent(null))),
          BlocProvider<SetEstimatedAmountBloc>(
              create: (_) => injector()..add(const SetAmountEvent(null))),
        ],
        child: Sizer(builder: (context, orientation, deviceType) {
          return KeyedSubtree(
            key: key,
            child: MaterialApp(
              localizationsDelegates: context.localizationDelegates,
              supportedLocales: context.supportedLocales,
              locale: context.locale,
              debugShowCheckedModeBanner: false,
              title: kMaterialAppTitle,
              onGenerateRoute: AppRoutes.onGenerateRoutes,
              theme: AppTheme.light,
              navigatorKey: MyApp.navigatorKey,
            ),
          );
        }));
  }
}
